import { Injectable } from '@angular/core';

import { LoginI } from '../../modelos/login.interface';
import { PsicologiaI } from '../../modelos/pisocologia.interface';
import { TutoriaI } from '../../modelos/tutoria.interface';
import { ResponseI } from '../../modelos/response.interface';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { Identifiers } from '@angular/compiler';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  login: string = "http://smartcampus.uniajc.edu.co:8888/RSU_LoginClient-0.0.2-SNAPSHOT/";

  notasPATH: string = "http://smartcampus.uniajc.edu.co:8888/rse_notasuniajc-2.0-SNAPSHOT/";

  usuariosPATH: string = "http://smartcampus.uniajc.edu.co:8991/rse_datosusuarios-1.0-SNAPSHOT/";

  seguimientoPATH: String = "http://smartcampus.uniajc.edu.co:8888/seguimiento-0.0.1-SNAPSHOT/";

  header = new HttpHeaders({
    'Authorization': 'eyJhbGciOiJIUzUxMiJ9.eyJhY2MiOjUwNTQwLCJzdWIiOiJmYWNpZnVlbnRlcyIsImNyZWF0ZWQiOjE1ODg2OTI0Nzg5NDgsImlhdCI6MTU4ODY5MjQ3OH0.eAZGmYNLs4F6Vut2a9eec1MMiAD6wFfHNp6tG0m-YnBRn9N1FwiKb39-I3j80a3pypRd-W1pspFxIUVL6mRPIQ'
  })
  constructor(private http: HttpClient) { }

  //LOGIN
  loginByUser(form: LoginI): Observable<ResponseI> {
    const headers = { 'content-type': 'application/json' }
    console.log(form)
    let direccion = this.login + 'login/api/authentication';
    return this.http.post<ResponseI>(direccion, form, { 'headers': headers });
  }

  recuperarByUser(form): Observable<ResponseI> {
    const headers = { 'content-type': 'application/json' }
    let direccion = this.login + 'login/api/olvideClave';
    return this.http.post<ResponseI>(direccion, form, { 'headers': headers });
  }

  //TRAE LOS USUARIOS POR CEDULA
  getDatosUsuario(identificacion): Observable<any> {
    let direccion = this.usuariosPATH + 'estudiante/showByIdentificacion/' + identificacion;
    return this.http
      .get(direccion, { 'headers': this.header });
  }

  getDatosUsuario2(identificacion) {
    let direccion = this.usuariosPATH + 'estudiante/showByIdentificacion/' + identificacion;
    return this.http
      .get(direccion, { 'headers': this.header }).pipe(map(response => response, err => err));
  }

  //TRAE TODAS LAS ACTIVIDADES
  getAllActividades(): Observable<Object> {
    const api = this.usuariosPATH + 'actividad/showAll';
    return this.http.get(api, { 'headers': this.header }).pipe(map(response => response, err => err));
  }

  //trae la actividad recibiendo por parametro el id
  getActividadesById(id_actividad): Observable<any> {
    let direccion = this.usuariosPATH + 'actividad/showByIdactividad/' + id_actividad;
    return this.http
      .get(direccion, { 'headers': this.header });
  }

  //TRAE TODAS LAS FRANJAS
  getAllFranjas(): Observable<any> {
    let direccion = this.usuariosPATH + 'franja/showAll';
    return this.http
      .get(direccion, { 'headers': this.header });
  }

  //TRAE TODAS LAS JORNADAS
  getAllJornadas(): Observable<any> {
    let direccion = this.usuariosPATH + 'jornada/showAll';
    return this.http
      .get(direccion, { 'headers': this.header });
  }

  getAllSedes(): Observable<any> {
    let direccion = this.usuariosPATH + 'sede/showAll';
    return this.http
      .get(direccion, { 'headers': this.header });
  }

  //OBTIENE EL HISTORICO DE NOTAS DE LOS ESTUDIANTES RECIEBIENDO COMO PARAMETRO PEGEID Y CODIGO DEL PROGRAMA
  getHistoricoNotas(pegeid, cod_programa): Observable<any> {
    let direccion = this.notasPATH + 'historiconotas/' + pegeid + '/' + cod_programa;
    return this.http
      .get(direccion, { 'headers': this.header });
  }

  // OBTIENE EL PROGRMA ACADEMICO POR ESTUDIANTE
  getPrograma(pegeid): Observable<any> {
    let direccion = this.notasPATH + 'programaacademico/' + pegeid;
    return this.http
      .get(direccion, { 'headers': this.header });
  }

  //*********************SEGUIMIENTO****************************************

  //MUESTRA TODAS LA ACTIVIDADES REG
  getActividadesSeguimiento(): Observable<Object> {
    const direccion = this.seguimientoPATH + 'activities/students/all/';
    return this.http.get(direccion, { 'headers': this.header }).pipe(map(response => response, err => err));
  }

  getActividadesSeguimientoId(id): Observable<Object> {
    const direccion = this.seguimientoPATH + 'activities/students/all/activities?activities='+id;
    return this.http.get(direccion, { 'headers': this.header }).pipe(map(response => response, err => err));
  }

  getActividadesSumaSeguimientoId(id): Observable<Object> {
    const direccion = this.seguimientoPATH + 'activities/students/report/activities?activities='+id;
    return this.http.get(direccion, { 'headers': this.header }).pipe(map(response => response, err => err));
  }
  putActividadesEstudiantes(id): Observable<Object> {
    const direccion = this.seguimientoPATH + 'activities/students/inactive/id?id='+id;
    return this.http.put(direccion, { 'headers': this.header }).pipe(map(response => response, err => err));
  }

  updateActividadesEstudiantes(form): Observable<Object> {
    const direccion = this.seguimientoPATH + 'activities/students/update';
    return this.http.put(direccion,form)
  }

  updateHorario(form): Observable<Object> {
    const direccion = this.seguimientoPATH + 'schedule/update';
    return this.http.put(direccion,form)
  }

  updateTutoria(form): Observable<Object> {
    const direccion = this.seguimientoPATH + 'tutorships/update';
    return this.http.put(direccion,form)
  }
  updateEstudianteTutoria(form): Observable<Object> {
    const direccion = this.seguimientoPATH + 'tutorships/students/update';
    return this.http.put(direccion,form)
  }
  setPsicologia(form:PsicologiaI):Observable<any>{
    let direccion=this.seguimientoPATH+'psychological/students/save/';
    return this.http.post<any>(direccion,form);
  }

  getPsicologiaByUser(id):Observable<Object> {
    const direccion = this.seguimientoPATH + 'psychological/students/all/users?users='+id;
    return this.http.get(direccion, { 'headers': this.header }).pipe(map(response => response, err => err));
    
  }

  getPsicologiaReportByStudents(documento):Observable<any> {
    const direccion = this.seguimientoPATH + 'psychological/students/all/students?students='+documento;
    return this.http.get(direccion).pipe(map(response => response, err => err));
    
  }

  getPsicologiaReportByOne(id, student, user):Observable<any> {
    const direccion = this.seguimientoPATH + 'psychological/students/report/id-students-users?id='+id+'&students='+student+'&users='+user;
    return this.http.get(direccion).pipe(map(response => response, err => err));
    
  }

  getTutoriaAllByUser(id,user,status):Observable<any> {
    const direccion = this.seguimientoPATH + 'tutorships/students/all/finished-tutorships-users?finished='+status+'&tutorships='+id+'&users='+user;
    return this.http.get(direccion).pipe(map(response => response, err => err));
    
  }
  //TRAE TUTORIAS DE ACUERDO AL USUARIO LOGUEADO
  getTuroriasByUSer(user,bool):Observable<any> {
    const direccion = this.seguimientoPATH + 'tutorships/all/finished-users?finished='+bool+'&users='+user;
    return this.http.get(direccion).pipe(map(response => response, err => err));
    
  }

  getTuroriasById(id,bool):Observable<any> {
    const direccion = this.seguimientoPATH + 'tutorships/finished-id?finished='+bool+'&id='+id;
    return this.http.get(direccion).pipe(map(response => response, err => err));
    
  }
  
  getNotasActuales(pegeid):Observable<any>{
    const direccion = this.notasPATH +'notasactual/'+pegeid;
    return this.http.get(direccion,{ 'headers': this.header }).pipe(map(response => response, err => err));
  }
  
  //ALAMCENAMIENTO DE TUTORIA
  setTutoria(form:TutoriaI):Observable<any>{
    let direccion=this.seguimientoPATH+'tutorships/save/';
    return this.http.post<any>(direccion,form);
  }
  setActividad(form):Observable<any>{
    let direccion=this.seguimientoPATH+'activities/students/save';
    return this.http.post<any>(direccion,form);
  }
  addStudentTutoria(form):Observable<any>{
    let direccion=this.seguimientoPATH+'tutorships/students/save/';
    return this.http.post<any>(direccion,form);
  }

  addAsistencia(form):Observable<any>{
    let direccion=this.seguimientoPATH+'attendance/save';
    return this.http.post<any>(direccion,form);
  }
  //GUARDAR HORARIO DE TUTORIA
  addHorarioTutoria(form):Observable<any>{
    let direccion=this.seguimientoPATH+'schedule/save';
    return this.http.post<any>(direccion,form);
  }

  getHorarioTutoria(id, docente):Observable<any>{
    const direccion = this.seguimientoPATH +'schedule/all/tutorships-users?tutorships='+id+'&users='+docente;
    return this.http.get(direccion,{ 'headers': this.header }).pipe(map(response => response, err => err));
  }
  putHorarioTutoriasEstudiantes(id): Observable<Object> {
    const direccion = this.seguimientoPATH + 'schedule/inactive/id?id='+id;
    return this.http.put(direccion, { 'headers': this.header }).pipe(map(response => response, err => err));
  }
  putTutoriasEstudiantes(id): Observable<Object> {
    const direccion = this.seguimientoPATH + 'tutorships/inactive/id?id='+id;
    return this.http.put(direccion, { 'headers': this.header }).pipe(map(response => response, err => err));
  }
  putCierreTutorias(id): Observable<Object> {
    const direccion = this.seguimientoPATH + 'tutorships/finished/id?id='+id;
    return this.http.put(direccion, { 'headers': this.header }).pipe(map(response => response, err => err));
  }

  putInactiveEstudentsTutorias(id): Observable<Object> {
    const direccion = this.seguimientoPATH + 'tutorships/students/inactive/id?id='+id;
    return this.http.put(direccion, { 'headers': this.header }).pipe(map(response => response, err => err));
  }

  putCerrarEntrevista(id): Observable<Object> {
    const direccion = this.seguimientoPATH + 'psychological/students/inactive/id?id='+id;
    return this.http.put(direccion, { 'headers': this.header }).pipe(map(response => response, err => err));
  }

  getReportActividadEstudiante(id):Observable<any>{
    const direccion = this.seguimientoPATH +'activities/students/report/students?students='+id;
    return this.http.get(direccion,{ 'headers': this.header }).pipe(map(response => response, err => err));
  }
}
