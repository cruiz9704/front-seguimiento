import { Component, OnInit } from '@angular/core';

import pdfMake from 'pdfmake/build/pdfmake';
import pdfFonts from 'pdfmake/build/vfs_fonts';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';
import { Utils } from 'src/app/utils/utils';
import { Router } from '@angular/router';

pdfMake.vfs = pdfFonts.pdfMake.vfs;

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  constructor(private router:Router) { }
  logoDataUrl: string;
  nombre:string = localStorage.getItem('full_name');
  pageid:string = localStorage.getItem('pageId');
  cc:string = localStorage.getItem('validateAdmin');
  bu:string = localStorage.getItem('validateBU');

  ngOnInit(): void {

    Utils.getImageDataUrlFromLocalPath1('assets/img/letras_azul.png').then(
      result => this.logoDataUrl = result
    )
    this.validartoken();

  }
  validartoken(){
    if(!localStorage.getItem('token')){
       this.router.navigate(['login'])
    }
  }

  openPdfStyleDict() {

    var documentDefinition = {
    content: [
      {
        image: this.logoDataUrl, style:'imagen'
      },
      { text: 'Consulta Psicologica', style: 'titulo' },
      { columns: 
        [
          {text: 'Datos Personales:', style:'cabecera'},
          {text: 'Grupo Familiar:', style:'cabecera'},
        ] 
      },
      {
        columns: [
          [{
            text: 'Codigo : ' + 1113536621, style: 'estudiante'
          },
          {
            text: 'Nombre : ' + 'Carlos Ruiz', style: 'estudiante'
          },
          {
            text: 'Sexo :' + 'Masculino', style: 'estudiante'
          },
          {
            text: 'Edad :' + '24', style: 'estudiante'
          },
          {
            text: 'Estado Civil :' + 'Soltero', style: 'estudiante'
          },
          {
            text: 'Hijos :' + '0', style: 'estudiante'
          },
          {
            text: 'Trabaja :' + 'si', style: 'estudiante'
          },
          {
            text: 'Empresa :' + 'Protecnica Ingeniera SAS', style: 'estudiante'
          }],

          
          {
            table: {
              headerRows: 1,
              widths: ['*', '*'],
    
              body: [
                [{
                  text:'Nombre',
                  style: 'tableHeader'
                }, {
                  text:'Parentezco',
                  style: 'tableHeader'
                }],
                ['Antonio', 'padre']
              ]
            }, style:'tabla'
          }
        ]
      },
      
    ],
    
    styles: {
      titulo: {
        fontSize: 35,
        bold: true,
        alignment: 'center',
        // margin: [left, top, right, bottom]
        margin: [0, -70, 0, 15]
      },
      tabla: {
        alignment: 'center'
      },
      imagen: {
        alignment: 'left'
      },
      tableHeader: {
        bold: true,
      },
      cabecera: {
        decoration: 'underline',
        bold:true,
        fontSize:18,
        margin: [0, 5, 0, 10]
      },
      estudiante: {
        margin: [0, 0, 0, 3]
      }
    }
  };
  pdfMake.createPdf(documentDefinition).open();
}

getExperienceObject(experiences) {

  const exs = [];

  experiences.forEach(experience => {
    exs.push(
      [{
        columns: [
          [{
            text: experience.jobTitle,
            style: 'jobTitle'
          },
          {
            text: experience.employer,
          },
          {
            text: experience.jobDescription,
          }],
          {
            text: 'Experience : ' + experience.experience + ' Months',
            alignment: 'right'
          }
        ]
      }]
    );
  });

  return {
    table: {
      widths: ['*'],
      body: [
        ...exs
      ]
    }
  };
}

}
