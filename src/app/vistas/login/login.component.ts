import { Component, OnInit } from '@angular/core';

import { FormGroup, FormControl, Validators  } from '@angular/forms';
import { ApiService } from '../../sevicios/api/api.service';

import { LoginI } from '../../modelos/login.interface';
import { Router } from '@angular/router';
import { ResponseI } from '../../modelos/response.interface'

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers:[ApiService]
})
export class LoginComponent implements OnInit {

  loginForm= new FormGroup({
    login : new FormControl('',Validators.required),
    password : new FormControl('',Validators.required)
  })
  recuperarForm= new FormGroup({
    username : new FormControl('',Validators.required)
  })
  constructor ( private api:ApiService, private router:Router){}

  errorStatus:boolean=false;
  errorMsj:string="";

  errorStatusRecuperar:boolean=false;
  errorMsjRecuperar:string="";
  errorStatusRecuperarEnvio:string="";

  ngOnInit(): void {
    this.validartoken();
  }

  validartoken(){
    if(localStorage.getItem('token')){
       this.router.navigate(['dashboard'])
    }else{
       this.router.navigate(['login'])
    }
  }

  onLogin(form:LoginI){
    this.api.loginByUser(form).subscribe((data)=>{
      let dataResponse : ResponseI = data;
      let validateAdmin='false';
      let validatePsicologo='false';
      let validateDocente='false';
      let validateDirector='false';
      let validateBecario='false';
      let validateBU='false';
      if(dataResponse.codigo==200){
          localStorage.setItem("token", dataResponse.valor['tokenAccesNew']);
          localStorage.setItem("identificacion", dataResponse.valor['identificacion']);
          localStorage.setItem("pageId", dataResponse.valor['pegeId']);
          localStorage.setItem("full_name", dataResponse.valor['primernombre']+" "+dataResponse.valor['primerapellido']);
          localStorage.setItem("usuario", dataResponse.valor['login']);

          //for(let i=0; i<=dataResponse.valor['listadoroles'].length; i++){
            for (const i in dataResponse.valor['listadoroles']) {

            let vlrId=dataResponse.valor['listadoroles'][i].vrolId;
           
              switch (vlrId) {
                case 306:{ // PROFESOR
                  validateDocente='true';   
                  localStorage.setItem("validateDocente", validateDocente);
                  break;
                }
                case 309:{ // BECARIO
                  validateBecario='true';   
                  localStorage.setItem("validateDocente", validateDocente);
                  break;
                }
                case 307:{ //PSICOLOGO
                  validatePsicologo='true';   
                  localStorage.setItem("validatePsicologo", validatePsicologo);
                  break;
                }
                case 308:{  //DIRECTOR
                  validateDirector='true';   
                  localStorage.setItem("validateDirector", validateDirector);
                  break;
                }
                case 304:{ //ADMINISTRADOR_SEGUIMIENTO
                  validateAdmin='true';   
                  localStorage.setItem("validateAdmin", validateAdmin);
                  break;
                }
                case 305:{ // ACADEMICO_BIENESTAR
                  validateBU='true';   
                  localStorage.setItem("validateBU", validateBU);
                  break;
                }
              }          
           
          }
            this.router.navigate(['dashboard'])
      }else{
           this.errorStatus=true;
           this.errorMsj = dataResponse.mensaje
      }
    })
  }

  onRecuperar(form){
    this.api.recuperarByUser(form).subscribe((data)=>{
      let dataResponse= data;
      switch (dataResponse.codigo) {
        case 200:{
          this.errorStatusRecuperar=false;
          this.errorMsjRecuperar = dataResponse.mensaje
          break;
        }
        case 401:{
          this.errorStatusRecuperar=true;
           this.errorMsjRecuperar = dataResponse.mensaje
          break;
        }
        case 402:{
          this.errorStatusRecuperar=true;
           this.errorMsjRecuperar = dataResponse.mensaje
          break;
        }
        case 404:{ 
          this.errorStatusRecuperar=true;
           this.errorMsjRecuperar = dataResponse.mensaje
          break;
        }
        case 500:{ 
          this.errorStatusRecuperar=true;
           this.errorMsjRecuperar = dataResponse.mensaje
          break;
        }
        case 403:{ 
          this.errorStatusRecuperar=true;
           this.errorMsjRecuperar = dataResponse.mensaje
          break;
        }
      }
    })
  }
}
