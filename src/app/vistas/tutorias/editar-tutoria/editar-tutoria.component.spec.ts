import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditarTutoriaComponent } from './editar-tutoria.component';

describe('EditarTutoriaComponent', () => {
  let component: EditarTutoriaComponent;
  let fixture: ComponentFixture<EditarTutoriaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditarTutoriaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditarTutoriaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
