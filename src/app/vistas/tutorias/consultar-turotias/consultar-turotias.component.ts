import { Component, OnInit,ViewChild  } from '@angular/core';

import { ApiService } from '../../../sevicios/api/api.service';
import { Observable, Subject } from 'rxjs';
import { ThrowStmt } from '@angular/compiler';

import pdfMake from 'pdfmake/build/pdfmake';
import pdfFonts from 'pdfmake/build/vfs_fonts';
import { Utils } from 'src/app/utils/utils';
import { bufferToggle } from 'rxjs/operators'
import Swal from 'sweetalert2';
import { FormGroup, FormControl, Validators  } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { ClockPickerDialogService, ClockPickerConfig } from 'ng-clock-picker-lib';
import { DataTableDirective } from 'angular-datatables';

pdfMake.vfs = pdfFonts.pdfMake.vfs;

@Component({
  selector: 'app-consultar-turotias',
  templateUrl: './consultar-turotias.component.html',
  styleUrls: ['./consultar-turotias.component.css']
})
export class ConsultarTurotiasComponent implements OnInit {

  config: ClockPickerConfig = { 
    wrapperClassName: 'className', 
    buttonCancel: "Cancelar",
    buttonConfirm: "Confirmar",
    closeOnOverlayClick: true,
    //initialValue: "",
  }

  logoDataUrl: string;
  selectedState:string;
  nombre:string = localStorage.getItem('full_name');
  pageid:string = localStorage.getItem('pageId');
  identificacion:string = localStorage.getItem('identificacion');
  admin:string = localStorage.getItem('validateAdmin');
  BU:string = localStorage.getItem('validateBU');
  psicologo:string = localStorage.getItem('validatePsicologo');
  director:string = localStorage.getItem('validateDirector');
  docente:string = localStorage.getItem('validateDocente');
  bandera:number=1;
  dataFranja:any;

  dtOptions: DataTables.Settings = {};
    dtTrigger = new Subject();
    data: any;
    horarios:any;
    datosPiscologia:any;
    subject = new Subject<any>();
    constructor(private api: ApiService, private router:Router, private activeroute:ActivatedRoute) { }

    submitFormUpdateHorario= new FormGroup({
      idSchedule: new FormControl('',Validators.required),
      weekday : new FormControl('',Validators.required),
      startTime : new FormControl('',Validators.required),
      endTime : new FormControl('',Validators.required),
      tutorships: new FormGroup({
        idTutorships : new FormControl('',Validators.required),
        users : new FormControl(this.identificacion,Validators.required),
      }),
    })
    ngOnInit(): void {
      this.validartoken();
       var groupColumn = 3;
        this.dtOptions = {
            pagingType: 'full_numbers',
            pageLength: 10,
            ordering: true,
            processing: true,
            searching: true,
            columnDefs: [
                { "visible": false, "targets": groupColumn }
            ],
            order: [[groupColumn, 'asc']],
            language: { url: '//cdn.datatables.net/plug-ins/1.10.25/i18n/Spanish.json' },
            drawCallback: function (settings) {
              var api = this.api();
              var rows = api.rows({ page: 'current' }).nodes();
              var last = null;
              api.column(groupColumn, { page: 'current' }).data().each(function (group, i) {
                  if (last !== group) {
                      $(rows).eq(i).before(
                          '<tr class="group"><td colspan="5" style="background-color:#0553a7; color: #fff; border: 1px solid #fff;"><b>' + group + '</b></td></tr>'
                      );
                      last = group;
                  }
              });
          }
        }, err => {
            console.log(err);
        }
        this.getListTutoria(this.identificacion)

        Utils.getImageDataUrlFromLocalPath1('assets/img/letras_azul.png').then(
          result => this.logoDataUrl = result
        )
        this.getSede();

    }
    @ViewChild(DataTableDirective, {static: false})
    dtElement: DataTableDirective;
  
    rerender(): void {
      this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
        dtInstance.destroy();
      });
    }
    validartoken(){
      if(!localStorage.getItem('token')){
         this.router.navigate(['login'])
      }
    }
    getSede(){
      this.api.getAllSedes().subscribe(
        (data)=>{
          this.dataFranja=data.valor;
        })
      }
    ngOnDestroy(): void {
        this.dtTrigger.unsubscribe();
    }

    OnSubmitUpHorarios(form){

      this.api.updateHorario(form).subscribe((data)=>{
        console.log(data)
        if(data['code']==201){
          Swal.fire(
            'Exito!',
            data['message'],
            'success'
          )
          this.getDatosTutoria(form.tutorships.idTutorships,form.tutorships.users);
         }
  
      }, err => {    
        console.log(err) 
        Swal.fire(
          'Error!',
          err.error.description,
         // 'Datos ingresados erroneamente',
          'error'
        )
       });
    }

    async getListTutoria(id){
      await  this.api.getTuroriasByUSer(id,false)
            .subscribe((res: any) => {
                this.data = res.data;
                this.dtTrigger.next();
            }, err => {
                console.log(err);
            });

  }

  async getListTutoriaFilter(id,bool){
    await  this.api.getTuroriasByUSer(id,bool)
          .subscribe((res: any) => {
              this.data = res.data;
              this.dtTrigger.next();
          }, err => {
              console.log(err);
          });

}

  getTutoriaByUser(){
    this.api.getTuroriasByUSer(this.identificacion,true).subscribe(
      (data)=>{
        //this.dataFranja=data.valor;
        console.log(data)
      })
    }

    getTutoriaByFilter(filter){
      if(filter=='true'){ // si tutptia esta finalizada
        this.rerender();
        this.getListTutoriaFilter(this.identificacion,true);
        this.bandera=2;
       
      }else if(filter=='false'){ // si tutoria no esta finalizada
        this.rerender();
        this.getListTutoriaFilter(this.identificacion,false);
        this.bandera=1;
       
      }
      
      }

      editarTutoria(id){
       this.router.navigate(['gestionarTutoria',id])
      }

      editarHorarioTutoria(id){
        this.router.navigate(['horarioTutorias',id])
       }

       getDatosTutoria(id,docente){
        this.api.getHorarioTutoria(id,docente).subscribe(
          (data)=>{
            this.horarios=data.data;
            console.log(data.data)
          } , err => {
            this.horarios=[];
            Swal.fire(
              'Error!',
              err.error.description,
              'error'
            )
          });
       }


       getDatosHorarioTutoria(id,docente,idSchedule) {
        this.api.getHorarioTutoria(id,docente)
           .subscribe((res: any) => {
               let item = res.data.filter(x => x.idSchedule == idSchedule);
              this.submitFormUpdateHorario.patchValue({
                idSchedule:item[0].idSchedule,
                weekday : item[0].weekday,
                startTime : item[0].startTime,
                endTime : item[0].endTime,
                tutorships:{
                  idTutorships: item[0].tutorships.idTutorships,
                }
              })
              console.log(item)
           }, err => {
               console.log(err);
           });
   }

       visibilidad(id,guardar,cancelar, visible) {        
        var elemento1 = document.querySelector(guardar);
        var elemento2 = document.querySelector(cancelar);
        if (elemento1 != null && elemento2 != null) {
          elemento1.style.display = visible?'inline-block':'none';
          elemento2.style.display = visible?'inline-block':'none';
          (<HTMLElement>document.querySelector('#editar'+id)).style.display=visible?'none':'inline-block';
          (<HTMLElement>document.querySelector('#eliminar'+id)).style.display=visible?'none':'inline-block';
          (<HTMLElement>document.querySelector('#text-inicio'+id)).style.display=visible?'none':'inline-block';
          (<HTMLElement>document.querySelector('#text-fin'+id)).style.display=visible?'none':'inline-block';
          (<HTMLElement>document.querySelector('#text-dia'+id)).style.display=visible?'none':'inline-block';

          (<HTMLElement>document.querySelector('#time-inicio'+id)).style.display=visible?'inline-block':'none';
          (<HTMLElement>document.querySelector('#dia'+id)).style.display=visible?'inline-block':'none';
          (<HTMLElement>document.querySelector('#time-fin'+id)).style.display=visible?'inline-block':'none';
        }
      }

      inactivarHorarioTutorias(id, id_tutoria, user){
        this.api.putHorarioTutoriasEstudiantes(id).subscribe((data)=>{
         if(data['code']==201){
          Swal.fire(
            'Exito!',
            data['message'],
            'success'
          )
          
          this.getDatosTutoria(id_tutoria, user);
          //this.router.navigate(['editarActividad',id])
         }
        }, err => {     
          Swal.fire(
            'Error!',
            err.error.description,
            'error'
          )
         });
      }

      inactivarTutorias(id){
        this.api.putTutoriasEstudiantes(id).subscribe((data)=>{
         if(data['code']==201){
          Swal.fire(
            'Exito!',
            data['message'],
            'success'
          )
          this.rerender()        
          this.getListTutoria(this.identificacion);
          
         }
        }, err => {     
          Swal.fire(
            'Error!',
            err.error.description,
            'error'
          )
         });
      }

      CerrarTutorias(id){
        this.api.putCierreTutorias(id).subscribe((data)=>{
         if(data['code']==201){
          Swal.fire(
            'Exito!',
            data['message'],
            'success'
          )
          this.rerender();
          this.getListTutoria(this.identificacion);
         }
        }, err => {     
          Swal.fire(
            'Error!',
            err.error.description,
            'error'
          )
         });
      }
      
       confirmacion(id_horario, id_tutoria, user){
        Swal.fire({
          title: '¿Estas seguro de eliminar este horario?',
          text: "una vez eliminada no podras revertir esta acción!",
          icon: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Si, eliminar!'
        }).then((result) => {
          if (result.isConfirmed) {
            this.inactivarHorarioTutorias(id_horario,id_tutoria, user);
          }
        })
       }

       confirmacionTutoria(id){
        Swal.fire({
          title: '¿Estas seguro de eliminar tutoria?',
          text: "una vez eliminada no podras revertir esta acción!",
          icon: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Si, cerrar!'
        }).then((result) => {
          if (result.isConfirmed) {
            this.inactivarTutorias(id);
          }
        })
       }
       confirmacionCierreTutoria(id){
        Swal.fire({
          title: '¿Estas seguro de cerrar la tutoria?',
          text: "una vez cerrada no podras intervenir, ni modificar datos de esta y tampoco revertir esta acción!",
          icon: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Si, cerrar!'
        }).then((result) => {
          if (result.isConfirmed) {
            this.CerrarTutorias(id);
          }
        })
       }
       async getDatosTutoriaReport(id,docente,bool){
        let respuesta;
        await  this.api.getTutoriaAllByUser(id,docente, bool)
              .toPromise().then((response) => {
                respuesta = response;
              }, err => {
                respuesta=err.error;
              });
              return respuesta;
      }

      async  consultarReportEstudiante(id){
        let respuestaActividadesReport;
        await this.api.getDatosUsuario(id)
            .toPromise().then(
            (response) => {
              respuestaActividadesReport = response;
            }, err => {
              respuestaActividadesReport=err.error;
            });
            return respuestaActividadesReport;
      }

      async  consultarReportTutoria(id,bool){
        let respuestaActividadesReport;
        await this.api.getTuroriasById(id,bool)
            .toPromise().then(
            (response) => {
              respuestaActividadesReport = response;
                //console.log(respuestaActividadesReport)
            }, err => {
              respuestaActividadesReport=err.error;
            });
            return respuestaActividadesReport;
      }

      async  consultarNotasActuales(pegeId){
        let respuestaActividadesReport;
        await this.api.getNotasActuales(pegeId)
            .toPromise().then(
            (response) => {
              respuestaActividadesReport = response;
            }, err => {
              respuestaActividadesReport=err.error;
            });
            return respuestaActividadesReport;
      }

      async filtrar(notas,dato){
        let item = notas.filter(x => x.codigoMateria == dato);
        
        return item;
      }
      
       async  consultarDocumentoTutorias(id, docente, bool){

        let datosEstudiante;
        let datosConsultaEstudiantes;
        let datosTutoria;
        let nombreEstudiantes;
        let nombreDocente;
        let notas;
        let prom1=0;
        let prom2
        let x:number;

        await this.getDatosTutoriaReport(id, docente, bool).then(response => {
          datosConsultaEstudiantes=response
        });
        await this.consultarReportTutoria(id,bool).then(response => {
            datosTutoria=response;
        });
        
        if(datosConsultaEstudiantes.code==200){
          datosConsultaEstudiantes=datosConsultaEstudiantes.data;
          datosTutoria=datosTutoria.data[0];
        
        for(let x=0 ;x<datosConsultaEstudiantes.length;x++){

          await this.consultarReportEstudiante(datosConsultaEstudiantes[x].students).then(response => {
            datosEstudiante=response.valor;
           
          });
          await this.consultarReportEstudiante(datosConsultaEstudiantes[x].tutorships.users).then(response => {
            nombreDocente=response.valor;
           
          });
          await this.consultarNotasActuales(datosEstudiante.pegeId).then(response => {
            notas=response.valor.programas[0];
            
           });
            let item = notas.materias.filter(f => f.codigoMateria === datosConsultaEstudiantes[x].subjects);

           
           if(item[0].calificaciones[0].evacDescripcion="NA"){

            datosConsultaEstudiantes[x].p1=item[0].calificaciones[0].evacDescripcion;
            datosConsultaEstudiantes[x].p2=item[0].calificaciones[0].evacDescripcion;

           }else if(item[0].calificaciones[0].evacDescripcion!="NA" && item[0].calificaciones.length==1){

            datosConsultaEstudiantes[x].p1=item[0].calificaciones[0].nota;
            datosConsultaEstudiantes[x].p2=item[0].calificaciones[0].evacDescripcion;

           }else if(item[0].calificaciones[0].evacDescripcion!="NA" && item[0].calificaciones.length>1){

            datosConsultaEstudiantes[x].p1=item[0].calificaciones[0].nota;
            datosConsultaEstudiantes[x].p2=item[0].calificaciones[1].evacDescripcion;
            
           }

           if(datosConsultaEstudiantes[x].noteOne>0 && datosConsultaEstudiantes[x].noteTwo==0 && datosConsultaEstudiantes[x].p1>0 && datosConsultaEstudiantes[x].p2=="NA"){

             prom1=((datosConsultaEstudiantes[x].noteOne+datosConsultaEstudiantes[x].p1)/2)

           }else if(datosConsultaEstudiantes[x].noteOne>0 && datosConsultaEstudiantes[x].noteTwo>0 && datosConsultaEstudiantes[x].p1=="NA" && datosConsultaEstudiantes[x].p2=="NA"){

            prom1=((datosConsultaEstudiantes[x].noteOne+datosConsultaEstudiantes[x].noteTwo)/2)

          }else if(datosConsultaEstudiantes[x].noteOne>0 && datosConsultaEstudiantes[x].noteTwo==0 && datosConsultaEstudiantes[x].p1=="NA" && datosConsultaEstudiantes[x].p2=="NA"){

            prom1=datosConsultaEstudiantes[x].noteOne;

          }
          else if(datosConsultaEstudiantes[x].noteOne==0 && datosConsultaEstudiantes[x].noteTwo==0 && datosConsultaEstudiantes[x].p1=="NA" && datosConsultaEstudiantes[x].p2=="NA"){

            prom1=0

          }else if(datosConsultaEstudiantes[x].noteOne>0 && datosConsultaEstudiantes[x].noteTwo>0 && datosConsultaEstudiantes[x].p1!="NA" && datosConsultaEstudiantes[x].p2!="NA"){

            prom1=((datosConsultaEstudiantes[x].noteOne+datosConsultaEstudiantes[x].noteTwo+datosConsultaEstudiantes[x].p1+datosConsultaEstudiantes[x].p2)/4)

          }
            nombreEstudiantes = datosEstudiante.primerNombre+' '+datosEstudiante.primerApellido+' '+datosEstudiante.segundoApellido
            nombreDocente = nombreDocente.primerNombre+' '+nombreDocente.primerApellido+' '+nombreDocente.segundoApellido

            datosConsultaEstudiantes[x].nombreEstudiante=nombreEstudiantes;
            datosConsultaEstudiantes[x].prom=prom1;
            datosConsultaEstudiantes[x].nombreMateria=item[0].descripMateria;
        }
        console.log(datosConsultaEstudiantes)
            var documentDefinition = {
                content: [
                  {
                    image: this.logoDataUrl, style:'imagen'
                  },
                  { text: 'Reporte de tutoria', style: 'titulo' },
                  { columns: 
                    [
                      {text: 'Nombre tutoria:', style:'cabecera'},
                    ] 
                  },
                  {
                    columns: [
                     [{
                       text: datosTutoria.description, style: 'estudiante'
                     }],
                    ]
                   },
                   { columns: 
                    [
                      {text: 'Sede:', style:'cabecera'},
                    ] 
                  },
                  {
                    columns: [
                     [{
                       text: datosTutoria.timeZone, style: 'estudiante'
                     }],
                    ]
                   },
                   { columns: 
                    [
                      {text: 'Docente o tutor:', style:'cabecera'},
                    ] 
                  },
                  {
                    columns: [
                     [{
                       text: nombreDocente, style: 'estudiante'
                     }],
                    ]
                   },
                  {
                    columns: [          
                      {
                        table: {
                          headerRows: 1,
                          widths: ['auto', 'auto','auto', 'auto','auto','auto', 'auto', 'auto', 'auto'],
                
                          body: [
                            [{
                              text:'Documento',
                              style: 'tableHeader'
                            }, 
                            {
                              text:'Nombre',
                              style: 'tableHeader'
                            },
                            {
                              text:'Materia',
                              style: 'tableHeader'
                            },
                            {
                              text:'Grupo',
                              style: 'tableHeader'
                            },
                            {
                              text:'Nota 1',
                              style: 'tableHeader'
                            },
                            {
                              text:'Nota 2', 
                              style: 'tableHeader'
                            },
                            {
                              text:'Parcial 1',
                              style: 'tableHeader'
                            },
                            {
                              text:'Parcial 2',
                              style: 'tableHeader'
                            }
                            ,
                            {
                              text:'Nota Promedio',
                              style: 'tableHeader'
                            }
                          ],
                         ...datosConsultaEstudiantes.map(i => {
                            return [i.students, i.nombreEstudiante, i.nombreMateria, i.group, i.noteOne,i.noteTwo,i.p1,i.p2,i.prom];
                          })                    
                          ]
                        }, style:'tabla'
                      }
                    ]
                    
                  },
                ],
                
                styles: {
                  titulo: {
                    fontSize: 30,
                    bold: true,
                    alignment: 'center',
                    // margin: [left, top, right, bottom]
                    margin: [0, -90, 0, 10],
                    color:'#0553a7',
                  },
                  firma: {
                    fontSize: 13,
                    alignment: 'left',
                    margin: [0, 25, 0, 0],
                  },
                  tabla: {
                    fontSize: 9,
                    alignment: 'center',
                    margin: [0, 0, 0, 0],
                  },
                  datos: {
                    fontSize: 12,
                    alignment: 'left',
                    margin: [0, 0, 0, 0],
                  },
                  imagen: {
                    alignment: 'left'
                  },
                  tableHeader: {
                    fillColor: '#555555',
                    color:'#ffffff',
                    bold: true,
                  },
                  cabecera: {
                    decoration: 'underline',
                    bold:true,
                    fontSize:15,
                    margin: [0, 5, 0, 5]
                  },
            
                  estudiante: {
                    margin: [0, 0, 0, 3],
                    fontSize: 13,
                  }
                }
              };
              pdfMake.createPdf(documentDefinition).open();
         }
        else if(datosConsultaEstudiantes.code==404){
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: datosConsultaEstudiantes.description
              })
        }
        //else if(datosActividad.code==404 && datosConsulta.codigo==200){
        //   Swal.fire({
        //       icon: 'error',
        //       title: 'Oops...',
        //       text: datosActividad.description
        //     })
      // }
        
    }  
}

