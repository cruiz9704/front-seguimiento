import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConsultarTurotiasComponent } from './consultar-turotias.component';

describe('ConsultarTurotiasComponent', () => {
  let component: ConsultarTurotiasComponent;
  let fixture: ComponentFixture<ConsultarTurotiasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConsultarTurotiasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConsultarTurotiasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
