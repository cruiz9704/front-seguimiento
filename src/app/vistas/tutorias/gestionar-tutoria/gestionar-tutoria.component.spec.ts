import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GestionarTutoriaComponent } from './gestionar-tutoria.component';

describe('GestionarTutoriaComponent', () => {
  let component: GestionarTutoriaComponent;
  let fixture: ComponentFixture<GestionarTutoriaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GestionarTutoriaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GestionarTutoriaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
