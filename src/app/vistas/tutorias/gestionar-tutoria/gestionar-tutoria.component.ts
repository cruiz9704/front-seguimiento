import { Component, OnInit,Input, ViewChild, ElementRef } from '@angular/core';

import { Router, ActivatedRoute } from '@angular/router';
import { from, Subject } from 'rxjs';
import { FormGroup, FormControl, Validators  } from '@angular/forms';
import { ApiService } from '../../../sevicios/api/api.service';
import Swal from 'sweetalert2'
import { ClockPickerDialogService, ClockPickerConfig } from 'ng-clock-picker-lib';
import { AmazingTimePickerService } from 'amazing-time-picker';
import { DataTableDirective } from 'angular-datatables';
@Component({
  selector: 'app-gestionar-tutoria',
  templateUrl: './gestionar-tutoria.component.html',
  styleUrls: ['./gestionar-tutoria.component.css'],
  providers: [ApiService]
})
export class GestionarTutoriaComponent implements OnInit {

  // @ViewChild('nota1') nota1: ElementRef;
  // @ViewChild('nota2') nota2: ElementRef;
  nombre:string = localStorage.getItem('full_name');
  pageid:string = localStorage.getItem('pageId');
  identificacion:string = localStorage.getItem('identificacion');
  admin:string = localStorage.getItem('validateAdmin');
  BU:string = localStorage.getItem('validateBU');
  psicologo:string = localStorage.getItem('validatePsicologo');
  director:string = localStorage.getItem('validateDirector');
  docente:string = localStorage.getItem('validateDocente');
  dataFranja:any;
  idTutoria:String=this.activeroute.snapshot.paramMap.get('id');;
  nombreEstudiante: String;
  ID:number;
  materias:any;
  dateTime:any;
  verElemento:boolean=false;
  

  dtOptions: DataTables.Settings = {};
  dtTrigger = new Subject();
  data: any;

  submitForm1= new FormGroup({
  users : new FormControl(this.identificacion,Validators.required),
  description : new FormControl('',Validators.required),
  timeZone : new FormControl('',Validators.required),
  idTutorships : new FormControl(this.activeroute.snapshot.paramMap.get('id'),Validators.required),
})

submitForm= new FormGroup({
  students : new FormControl('',Validators.required),
  group : new FormControl('',Validators.required),
  grupo : new FormControl({ value: '', disabled: true },Validators.required),
  noteOne : new FormControl('0',Validators.required),
  noteTwo : new FormControl('0',Validators.required),
  subjects : new FormControl('',Validators.required),
  materia : new FormControl('',Validators.required),
  tutorships: new FormGroup({
    idTutorships : new FormControl(this.activeroute.snapshot.paramMap.get('id'),Validators.required),
    users : new FormControl(this.identificacion,Validators.required),
  }),
  
})

submitFormNotas= new FormGroup({
  students : new FormControl('',Validators.required),
  idTutorshipsStudents : new FormControl('',Validators.required),
  group : new FormControl('',Validators.required),
  noteOne : new FormControl('',Validators.required),
  noteTwo : new FormControl('0',Validators.required),
  subjects : new FormControl('',Validators.required),
  tutorships: new FormGroup({
    idTutorships : new FormControl(this.activeroute.snapshot.paramMap.get('id'),Validators.required),
    users : new FormControl(this.identificacion,Validators.required),
  }),
  
})
submitFormHorario= new FormGroup({
  weekday : new FormControl('',Validators.required),
  startTime : new FormControl('',Validators.required),
  endTime : new FormControl('',Validators.required),
  lunes : new FormControl('',Validators.required),
  martes : new FormControl('',Validators.required),
  miercoles : new FormControl('',Validators.required),
  jueves : new FormControl('',Validators.required),
  viernes : new FormControl('',Validators.required),
  sabado : new FormControl('',Validators.required),
  domingo : new FormControl('',Validators.required),
  tutorships: new FormGroup({
    idTutorships : new FormControl(this.activeroute.snapshot.paramMap.get('id'),Validators.required),
    users : new FormControl(this.identificacion,Validators.required),
  }),
})

config: ClockPickerConfig = { 
  wrapperClassName: 'className', 
  buttonCancel: "Cancelar",
  buttonConfirm: "Confirmar",
  closeOnOverlayClick: true,
  //initialValue: "",
}

  constructor(private api:ApiService, private router:Router, private activeroute:ActivatedRoute) { }

  
  ngOnInit(): void {
    this.validartoken();
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 5,
      language: { url: '//cdn.datatables.net/plug-ins/1.10.25/i18n/Spanish.json' }
    }, err => {
        console.log(err);
    }
    
    this.getAllTutorias();
     this.idTutoria = this.activeroute.snapshot.paramMap.get('id');
    this.getTutoriaId(this.idTutoria);
    this.getSede();
    
  }
  @ViewChild(DataTableDirective, {static: false})
    dtElement: DataTableDirective;
  
    rerender(): void {
      this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
        dtInstance.destroy();
      });
    }
  ngOnDestroy(): void {
    this.dtTrigger.unsubscribe();
}

  validartoken(){
    if(!localStorage.getItem('token')){
       this.router.navigate(['login'])
    }
  }
  getSede(){
    this.api.getAllSedes().subscribe(
      (data)=>{
        this.dataFranja=data.valor;
      })
    } 

    getTutoriaId(id){
      this.api.getTuroriasById(id,false).subscribe(
        (data)=>{ 
          this.submitForm1.patchValue({
            description : data.data[0].description,
            timeZone : data.data[0].timeZone
          })
        }, err => {
          console.log(err);
      });
    }

    async getDatosTutoria(id,docente){
      let respuesta;
      await  this.api.getTutoriaAllByUser(id,docente, false)
            .toPromise().then((response) => {
              respuesta = response;
            }).catch(e => console.error(e));
            return respuesta;
    }

   async OnSubmitNotas(id, idTutoria, docente){
     
      let nota1 =(<HTMLInputElement>document.getElementById("nota1"+id)).value;
      let nota2 =(<HTMLInputElement>document.getElementById("nota2"+id)).value;      ;
      let form:any;
      let datosConsulta;
      await this.getDatosTutoria(idTutoria, docente).then(response => {
        datosConsulta=response.data;
      });

       let item = datosConsulta.filter(x => x.idTutorshipsStudents == id);
        form={
        group: item[0].group,
        idTutorshipsStudents: item[0].idTutorshipsStudents,
        noteOne: nota1,
        noteTwo: nota2,
        students: item[0].students,
        subjects: item[0].subjects,
        tutorships:{
          idTutorships: item[0].tutorships.idTutorships,
          users:item[0].tutorships.users
        }
      }
      this.updateNotas(form);
    }

    updateNotas(form){  
      console.log(form)  
      this.api.updateEstudianteTutoria(form).subscribe((data)=>{
        console.log(data)
       if(data['code']==201){

        Swal.fire(
          'Exito!',
          data['message'],
          'success'
        )
  
       }
      }, err => {     
        Swal.fire(
          'Error!',
          err.error.description,
          'error'
        )
       });
    }
  OnSubmitUpdate(form){
    console.log(form)
    //updateTutoria
    this.api.updateTutoria(form).subscribe((data)=>{
      console.log(data)
     if(data['code']==201){
       this.resetForm2()
      Swal.fire(
        'Exito!',
        data['message'],
        'success'
      )

     }
    }, err => {     
      Swal.fire(
        'Error!',
        err.error.description,
        'error'
      )
     });
  }

  OnSubmit(form){
    //console.log(form)
      this.api.addStudentTutoria(form).subscribe((data)=>{
        console.log(data)
       if(data.code==201){
         this.resetForm2()
         this.rerender();
         this.getAllTutorias();
        Swal.fire(
          'Exito!',
          data.message,
          'success'
        )
  
       }
      }, err => {   
        console.log(err)  
        Swal.fire(
          'Error!',
          err.error.description,
          'error'
        )
       });
    }
  addHorarioTutoria(form){
    console.log(form)
    this.api.addHorarioTutoria(form).subscribe((data)=>{
      console.log(data)
     if(data.code==201){
       //this.resetForm2()
      Swal.fire(
        'Exito!',
        data.message,
        'success'
      )

     }
    }, err => {     
      Swal.fire(
        'Error!',
        err.error.description,
        'error'
      )
     });
  }
    OnSubmitHorarios(form){
      
    let concatenar="";
    if(form.lunes){
      form.weekday="Lunes"
      this.addHorarioTutoria(form);
    }
    if(form.martes){
      form.weekday="Martes"
      this.addHorarioTutoria(form);
      /*if(concatenar!=''){
        concatenar+=',Martes';
      }else{
        concatenar='Martes';
      }*/
     
    }
    if(form.miercoles){
      form.weekday="Miercoles"
      this.addHorarioTutoria(form);
    }
    if(form.jueves){
      form.weekday="Jueves"
      this.addHorarioTutoria(form);
    }
    if(form.viernes){
      form.weekday="Viernes"
      this.addHorarioTutoria(form);
    }
    if(form.sabado){
      form.weekday="Sabado"
      this.addHorarioTutoria(form);
    }
    if(form.domingo){
      form.weekday="Domingo"
      this.addHorarioTutoria(form);
    }
    }
  resetForm(){
    this.submitForm1.patchValue({
      students : '',
      group : '',
      noteOne : '',
      noteTwo : '',
      subjects : '',
    })
  }

  resetForm2(){
    this.submitForm.patchValue({
      students :'',
      group : '',
      grupo : '',
      subjects : '',
      materia : '',
    })
  }

  async getDatosStudent(student){
    let respuesta;
    await  this.api.getDatosUsuario(student)
          .toPromise().then((response) => {
            respuesta = response;
          }, err => {
            this.materias=[];
            this.resetForm2();
            respuesta=err.error;
          });
          return respuesta;
  }
  
   getIdentificacion(id){
      this.api.getDatosUsuario(id).subscribe(
        (data)=>{            
          console.log(data)
        },
        (error)=>{ 
            Swal.fire(
            'Error!',
            error.error.mensaje,
            'error'
          )
        }
      )
     }
     getGroupByMateria(event: any){
      this.ID=event.target.value;
     }
     getNotas(pegeid){
      this.api.getNotasActuales(pegeid).subscribe(
        (data)=>{  
          if(data.valor['programas']==null){
            this.resetForm2();
            this.materias=[];
            Swal.fire(
              'Error!',
              'El estudiante no registra materias',
              'error'
            )
          }else{
            this.materias=data.valor['programas'][0].materias
          }
          
        },
        (error)=>{ 
            Swal.fire(
            'Error!',
            error.error.mensaje,
            'error'
          )
        }
      )
     }

     changeNotas(event: any){
       let datos = event.target.value;
       let arreglo = datos.split(',');

       this.submitForm.patchValue({
          group : arreglo[1],
          grupo : arreglo[1],
          subjects : arreglo[0],
        })
       console.log(datos.split(','))
     }

  async getAllData(event: any){
    let datosEstudiante;

    this.ID=event.target.value;
    this.getIdentificacion(this.ID)

    await this.getDatosStudent(this.ID).then(response => {
      datosEstudiante=response.valor
    });

    console.log(datosEstudiante.pegeId)
    this.getNotas(datosEstudiante.pegeId);
  }

  // getTutoriaId(id){
  //   this.api.getTuroriasById(id,false).subscribe(
  //     (data)=>{ 
  //       this.submitForm1.patchValue({
  //         description : data.data[0].description,
  //         timeZone : data.data[0].timeZone
  //       })
  //     }, err => {
  //       console.log(err);
  //   });
  // }

  async getAllTutorias() {
    await this.api.getTutoriaAllByUser(this.idTutoria,this.identificacion,'false')
        .subscribe((res: any) => {
            this.data = res.data;
            this.dtTrigger.next();
            console.log(this.data)
        }, err => {
            console.log(err);
        });
}

InactivarEstudianteTutorias(id){
  this.api.putInactiveEstudentsTutorias(id).subscribe((data)=>{
   if(data['code']==201){
    Swal.fire(
      'Exito!',
      data['message'],
      'success'
    )
    this.rerender();
    this.getAllTutorias();
   }
  }, err => {     
    Swal.fire(
      'Error!',
      err.error.description,
      'error'
    )
   });
}

 confirmacion(id){
  Swal.fire({
    title: '¿Estas seguro de eliminar el estudiante?',
    text: "una vez eliminad@ no podras revertir esta acción!",
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Si, eliminar!'
  }).then((result) => {
    if (result.isConfirmed) {
      this.InactivarEstudianteTutorias(id);
    }
  })
 }

 visibilidad(visible) {  


  if(visible){
    this.verElemento=true
  }else{
    this.verElemento=false
  }
}
setAsistencia(form, idtutoriaEstudiante){  
  console.log(form)  
  this.api.addAsistencia(form).subscribe((data)=>{
    console.log(data)
   if(data['code']==201){

    Swal.fire(
      'Exito!',
      data['message'],
      'success'
    );
    (<HTMLElement>document.querySelector('#asisitio'+idtutoriaEstudiante)).style.display='none';
    (<HTMLElement>document.querySelector('#no-asisitio'+idtutoriaEstudiante)).style.display='none';
   }
  }, err => {     
    Swal.fire(
      'Error!',
      err.error.description,
      'error'
    )
   });
  }
asistencia(idtutoriaEstudiante,estudiante,tutoria,docente, bool){
  let form:any;
  
  if(this.dateTime==undefined || this.dateTime=="" || this.dateTime==null){
    Swal.fire(
      'Error!',
      'Por favor valide la fecha de la asistencia',
      'error'
    )
    document.getElementById("date-time").focus();
  }else{
    if(bool){

      form={
        attendanceDateTime: this.dateTime,
        attended: true,
        tutorshipsStudents: {
          idTutorshipsStudents: idtutoriaEstudiante,
          students: estudiante,
          tutorships: {
            idTutorships: tutoria,
            users: docente
          }
        }
      }

      this.setAsistencia(form,idtutoriaEstudiante);
    }else{
      form={
        attendanceDateTime: this.dateTime,
        attended: false,
        tutorshipsStudents: {
          idTutorshipsStudents: idtutoriaEstudiante,
          students: estudiante,
          tutorships: {
            idTutorships: tutoria,
            users: docente
          }
        }
      }

      this.setAsistencia(form,idtutoriaEstudiante);
    }
  }
  
}  

}

