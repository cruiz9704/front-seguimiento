import { Component, OnInit } from '@angular/core';

import { FormGroup, FormControl, Validators  } from '@angular/forms';
import { ApiService } from '../../../sevicios/api/api.service';
import { TutoriaI } from '../../../modelos/tutoria.interface';

import Swal from 'sweetalert2'
import { Router } from '@angular/router';

@Component({
  selector: 'app-crear-tutoria',
  templateUrl: './crear-tutoria.component.html',
  styleUrls: ['./crear-tutoria.component.css'],
  providers:[ApiService]
})
export class CrearTutoriaComponent implements OnInit {

  nombre:string = localStorage.getItem('full_name');
  pageid:string = localStorage.getItem('pageId');
  admin:string = localStorage.getItem('validateAdmin');
  BU:string = localStorage.getItem('validateBU');
  psicologo:string = localStorage.getItem('validatePsicologo');
  director:string = localStorage.getItem('validateDirector');
  docente:string = localStorage.getItem('validateDocente');
  identificacion:string = localStorage.getItem('identificacion');
  dataJornada:any;
  dataFranja:any;
  formTutoria:any;

  submitForm= new FormGroup({
    users : new FormControl(this.identificacion,Validators.required),
    timeZone : new FormControl('',Validators.required),
    description: new FormControl('',Validators.required)
  })

  constructor(private api:ApiService, private router:Router) { }

  ngOnInit(): void {
    this.getSede();
    this.validartoken();
  }

  validartoken(){
    if(!localStorage.getItem('token')){
       this.router.navigate(['login'])
    }
  }
  OnSubmit(form:TutoriaI){
    let formTutoria : TutoriaI = form;
    this.api.setTutoria(formTutoria).subscribe((data)=>{
      console.log(data)
     if(data.code==201){
       this.resetForm()
      Swal.fire(
        'Exito!',
        data.message,
        'success'
      )

     }
    }, err => {     
      Swal.fire(
        'Error!',
        err.error.description,
        'error'
      )
     });
  }

   resetForm(){
    this.submitForm.patchValue({
      timeZone : '',
      description : ''
    })
  }
  getJornada(){
    this.api.getAllJornadas().subscribe(
      (data)=>{
      console.log( this.dataJornada=data.valor);
      })
    } 
    getSede(){
      this.api.getAllSedes().subscribe(
        (data)=>{
          this.dataFranja=data.valor;
        })
      }  
}
