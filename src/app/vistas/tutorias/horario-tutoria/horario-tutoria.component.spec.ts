import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HorarioTutoriaComponent } from './horario-tutoria.component';

describe('HorarioTutoriaComponent', () => {
  let component: HorarioTutoriaComponent;
  let fixture: ComponentFixture<HorarioTutoriaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HorarioTutoriaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HorarioTutoriaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
