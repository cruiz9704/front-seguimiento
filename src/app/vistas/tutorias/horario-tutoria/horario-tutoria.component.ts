import { Component, OnInit } from '@angular/core';

import { Router, ActivatedRoute } from '@angular/router';
import { Subject } from 'rxjs';
import { FormGroup, FormControl, Validators  } from '@angular/forms';
import { ApiService } from '../../../sevicios/api/api.service';
import { ClockPickerDialogService, ClockPickerConfig } from 'ng-clock-picker-lib'

import Swal from 'sweetalert2'

@Component({
  selector: 'app-horario-tutoria',
  templateUrl: './horario-tutoria.component.html',
  styleUrls: ['./horario-tutoria.component.css'],
  providers: [ApiService]
})
export class HorarioTutoriaComponent implements OnInit {
  submitForm= new FormGroup({
    time : new FormControl('',Validators.required),
  })

  config: ClockPickerConfig = { 
    wrapperClassName: 'className', 
    buttonCancel: "Cancelar",
    buttonConfirm: "Confirmar",
    closeOnOverlayClick: true,
    //initialValue: "",
  }
  constructor(private api:ApiService, private router:Router, private activeroute:ActivatedRoute) { }

  ngOnInit(): void {
  }

  OnSubmit(){}

}
