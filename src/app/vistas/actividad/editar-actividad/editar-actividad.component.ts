import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../../sevicios/api/api.service';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl, Validators  } from '@angular/forms';
import Swal from 'sweetalert2';
import {Location} from '@angular/common';
@Component({
  selector: 'app-editar-actividad',
  templateUrl: './editar-actividad.component.html',
  styleUrls: ['./editar-actividad.component.css'],
  providers: [ApiService]
})
export class EditarActividadComponent implements OnInit {

  submitForm= new FormGroup({
    estudiante : new FormControl({ value: '', disabled: true },[Validators.required]),
    activities : new FormControl('',Validators.required),
    attendanceDate : new FormControl('',Validators.required),
    completedHours : new FormControl('',Validators.required),
    students : new FormControl('',Validators.required),
    idActivitiesStudents : new FormControl(parseInt(this.activeroute.snapshot.paramMap.get('id')),Validators.required),
    nombre : new FormControl({ value: '', disabled: true },[Validators.required]),
    facultad : new FormControl({ value: '', disabled: true },Validators.required),
    programa : new FormControl({ value: '', disabled: true },Validators.required),
    actividad : new FormControl({ value: '', disabled: true },Validators.required),
  })
 
  constructor ( private api:ApiService, private router:Router, private activeroute:ActivatedRoute,private _location: Location){}

  nombre:string = localStorage.getItem('full_name');
  pageid:string = localStorage.getItem('pageId');
  admin:string = localStorage.getItem('validateAdmin');
  BU:string = localStorage.getItem('validateBU');
  psicologo:string = localStorage.getItem('validatePsicologo');
  director:string = localStorage.getItem('validateDirector');
  docente:string = localStorage.getItem('validateDocente');
  ID:number;
  dataFamiliares:any=[{}];
  idActividadEstudiante:any=this.activeroute.snapshot.paramMap.get('id');
  ngOnInit(): void {
    this.getActividades();
    this.validartoken();
  }

  validartoken(){
    if(!localStorage.getItem('token')){
       this.router.navigate(['login'])
    }
  }

  OnSubmit(form){
     console.log(form);

    this.api.updateActividadesEstudiantes(form).subscribe((data)=>{
      if(data['code']==201){
        Swal.fire(
          'Exito!',
          data['message'],
          'success'
        )
        //this.router.navigate(['editarActividad',id])
       }

    }, err => {    
      console.log(err) 
      Swal.fire(
        'Error!',
        err.error.message,
       // 'Datos ingresados erroneamente',
        'error'
      )
     });
  }

  getDatosPrograma(pegeid){
    this.api.getPrograma(pegeid).subscribe(
      (data)=>{
        //console.log(data)
        if(data['valor'].length>0){
          this.submitForm.patchValue({
            facultad:"Ingeniera",
            programa:data['valor'][0].nombrePrograma
          })
        }else{
          this.submitForm.patchValue({
            facultad:"Ingeniera",
            programa:"No registra"
          })
        }
        
      });
  }
  getIdentificacion(identificacion){
    this.api.getDatosUsuario(identificacion).subscribe(
      (data)=>{   
        //console.log(data)         
        this.dataFamiliares=data.valor['familiares'];
        let nombre_completo= data.valor.primerNombre+' '+data.valor.primerApellido+' '+data.valor.segundoApellido;
        
        this.submitForm.patchValue({
          estudiante:identificacion,
          students:identificacion,
          nombre:nombre_completo
        })
        this.getDatosPrograma(data.valor.pegeId)
      },err => {     
        Swal.fire(
          'Error!',
          err.error.mensaje,
          'error'
        )
       });
  }
   getActividades() {
     this.api.getActividadesSeguimiento()
        .subscribe((res: any) => {
            let item = res.data.filter(x => x.idActivitiesStudents == this.idActividadEstudiante);
            this.getIdentificacion(item[0].students)
            this.getActividadId(item[0].activities)
            this.submitForm.patchValue({
              attendanceDate:item[0].attendanceDate,
              completedHours:item[0].completedHours,
            })
        }, err => {
            console.log(err);
        });
}
  
 
  
  


 getActividadId(id_actividad){
  this.api.getActividadesById(id_actividad).subscribe(
    (data)=>{ 
      this.submitForm.patchValue({
        activities:id_actividad,
        actividad:data.valor.actividad,
      })
    },err => {     
      Swal.fire(
        'Error!',
        'No se encontro actividad con el id: '+this.ID,
        'error'
      )
     });
 }

 getActividadFind(event: any){
  this.ID=event.target.value;
  this.api.getActividadesById(this.ID).subscribe(
    (data)=>{ 
      this.submitForm.patchValue({
        actividad:data.valor.actividad,
      })
    },err => {     
      Swal.fire(
        'Error!',
        'No se encontro actividad con el id: '+this.ID,
        'error'
      )
     });
 }

 backClicked() {
  this._location.back();
}

}
