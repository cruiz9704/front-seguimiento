import { Component, OnInit, OnDestroy,ViewChild } from '@angular/core';
import { ApiService } from '../../../sevicios/api/api.service';
import { Subject } from 'rxjs';
import { Router } from '@angular/router';
import pdfMake from 'pdfmake/build/pdfmake';
import pdfFonts from 'pdfmake/build/vfs_fonts';
import { Utils } from 'src/app/utils/utils';
import Swal from 'sweetalert2';
import { DataTableDirective } from 'angular-datatables';

@Component({
    selector: 'app-consultar-actividades',
    templateUrl: './consultar-actividades.component.html',
    styleUrls: ['./consultar-actividades.component.css'],
    providers: [ApiService]
})
export class ConsultarActividadesComponent implements OnInit, OnDestroy {

    dtOptions: DataTables.Settings = {};
    dtTrigger = new Subject();
    data: any;
    logoDataUrl: string;
    document: string;
    constructor(private api: ApiService, private router:Router) { }

    ngOnInit(): void {
      var groupColumn = 2;
        this.dtOptions = {
            pagingType: 'full_numbers',
            pageLength: 10,
            ordering: true,
            processing: true,
            searching: true,
            columnDefs: [
                { "visible": false, "targets": groupColumn }
            ],
            order: [[groupColumn, 'asc']],
            language: { url: '//cdn.datatables.net/plug-ins/1.10.25/i18n/Spanish.json' },
            drawCallback: function (settings) {
              var api = this.api();
              var rows = api.rows({ page: 'current' }).nodes();
              var last = null;
              api.column(groupColumn, { page: 'current' }).data().each(function (group, i) {
                  if (last !== group) {
                      $(rows).eq(i).before(
                          '<tr class="group"><td colspan="5" style="background-color:#0553a7; color: #fff; border: 1px solid #fff;"><b>' + group + '</b></td></tr>'
                      );
                      last = group;
                  }
              });
          }
        }, err => {
            console.log(err);
        }
        this.getActividades();
        Utils.getImageDataUrlFromLocalPath1('assets/img/letras_azul.png').then(
            result => this.logoDataUrl = result
          )
          this.validartoken();
    }
    @ViewChild(DataTableDirective, {static: false})
    dtElement: DataTableDirective;
  
    rerender(): void {
      this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
        dtInstance.destroy();
      });
    }

    validartoken(){
      if(!localStorage.getItem('token')){
         this.router.navigate(['login'])
      }
    }

    ngOnDestroy(): void {
        this.dtTrigger.unsubscribe();
    }

    async getActividades() {
        await this.api.getAllActividades()
            .subscribe((res: any) => {
                this.data = res.valor;
                this.dtTrigger.next();
                //console.log(this.data)
            }, err => {
                console.log(err);
            });
    }


     agergarActividad(id){  
        this.router.navigate(['registrarActividad',id])
    }

    listarEstudiantes(id,name){  
        this.router.navigate(['actividadesEstudiantes',id,name])
    }
    
    async geDataReportActividad(id){
        let respuesta;
        await  this.api.getActividadesById(id)
              .toPromise().then(
                  (response) => {
                     respuesta = response;
              }).catch(e => console.error(e));
              return respuesta;
      }
      async getActividadesReportId(id) {
          let respuestaActividades;
        await this.api.getActividadesSeguimientoId(id)
            .toPromise().then(
            (response) => {
                respuestaActividades = response;
                //console.log(respuestaActividades)
            }, err => {
                respuestaActividades=err.error;
            });
            return respuestaActividades;
      }

      async getActividadesSumaReportId(id) {
        let respuestaActividades;
      await this.api.getActividadesSumaSeguimientoId(id)
          .toPromise().then(
          (response) => {
              respuestaActividades = response;
              //console.log(respuestaActividades)
          }, err => {
              respuestaActividades=err.error;
          });
          return respuestaActividades;
    }

    async openPdfReport(id){
        let datosConsulta;
        let datosActividad;
        let datosEstudiante;
        let x:number;

        await this.geDataReportActividad(id).then(response => {
          datosConsulta=response.valor
        });
        await this.getActividadesReportId(id).then(response => {
            datosActividad=response;
          });
            if(datosActividad.code==200){
                datosActividad=datosActividad.data;

                for(let x=0 ;x<datosActividad.length;x++){
                  console.log(datosActividad[x].students)
                  await this.getDatosStudent(datosActividad[x].students).then(response => {
                    datosEstudiante=response.valor;
                   
                  });
                  let nombreEstudiantes = datosEstudiante.primerNombre+' '+datosEstudiante.primerApellido+' '+datosEstudiante.segundoApellido;
                  datosActividad[x].nombreEstudiante=nombreEstudiantes;
                }
                var documentDefinition = {
                    content: [
                      {
                        image: this.logoDataUrl, style:'imagen'
                      },
                      { text: 'Reporte de actividad detallado', style: 'titulo' },
                      { columns: 
                        [
                          {text: 'Nombre actividad:', style:'cabecera'},
                        ] 
                      },
                      {
                        columns: [
                         [{
                           text: datosConsulta.actividad, style: 'estudiante'
                         }],
                        ]
                       },
                      { 
                        columns: 
                        [
                          {text: 'Tipo:', style:'cabecera'}
                        ] 
                      },
                      {
                       columns: [
                        [{
                          text: datosConsulta.tipoActividad, style: 'estudiante'
                        }],
                       ]
                      },
                      {
                        columns: [          
                          {
                            table: {
                              headerRows: 1,
                              widths: ['*', '*','*','*'],
                    
                              body: [
                                [{
                                  text:'Documento Estudiante',
                                  style: 'tableHeader'
                                }, 
                                {
                                  text:'Nombre Estudiante',
                                  style: 'tableHeader'
                                }, 
                                {
                                  text:'Horas',
                                  style: 'tableHeader'
                                },
                                {
                                  text:'Fecha',
                                  style: 'tableHeader'
                                }
                              ],
                             ...datosActividad.map(i => {
                                return [i.students, i.nombreEstudiante,i.completedHours, i.attendanceDate];
                              })
                             
                              ]
                            }, style:'tabla'
                          }
                        ]
                      },
                    ],
                    
                    styles: {
                      titulo: {
                        fontSize: 30,
                        bold: true,
                        alignment: 'center',
                        // margin: [left, top, right, bottom]
                        margin: [0, -90, 0, 10],
                        color:'#0553a7',
                      },
                      firma: {
                        fontSize: 13,
                        alignment: 'left',
                        margin: [0, 25, 0, 0],
                      },
                      tabla: {
                        fontSize: 9,
                        alignment: 'center',
                        margin: [0, 0, 0, 0],
                      },
                      datos: {
                        fontSize: 12,
                        alignment: 'left',
                        margin: [0, 0, 0, 0],
                      },
                      imagen: {
                        alignment: 'left'
                      },
                      tableHeader: {
                        bold: true,
                      },
                      cabecera: {
                        decoration: 'underline',
                        bold:true,
                        fontSize:15,
                        margin: [0, 5, 0, 5]
                      },
                
                      estudiante: {
                        margin: [0, 0, 0, 3],
                        fontSize: 11,
                      }
                    }
                  };
                  pdfMake.createPdf(documentDefinition).open();
            }else if(datosActividad.code==404){
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: datosActividad.description
                  })
            }
      }

      async openPdfReportSuma(id){
        let datosConsulta;
        let datosActividad;
        let datosEstudiante;
        let x:number;

        await this.geDataReportActividad(id).then(response => {
          datosConsulta=response.valor
        });
        await this.getActividadesSumaReportId(id).then(response => {
            datosActividad=response;
          });
            if(datosActividad.code==200){
                datosActividad=datosActividad.data;

                for(let x=0 ;x<datosActividad.length;x++){
                  console.log(datosActividad[x].students)
                  await this.getDatosStudent(datosActividad[x].students).then(response => {
                    datosEstudiante=response.valor;
                   
                  });
                  let nombreEstudiantes = datosEstudiante.primerNombre+' '+datosEstudiante.primerApellido+' '+datosEstudiante.segundoApellido;
                  datosActividad[x].nombreEstudiante=nombreEstudiantes;
                }
                console.log(datosActividad)
                var documentDefinition = {
                    content: [
                      {
                        image: this.logoDataUrl, style:'imagen'
                      },
                      { text: 'Reporte de actividad sumatoria', style: 'titulo' },
                      { columns: 
                        [
                          {text: 'Nombre actividad:', style:'cabecera'},
                        ] 
                      },
                      {
                        columns: [
                         [{
                           text: datosConsulta.actividad, style: 'estudiante'
                         }],
                        ]
                       },
                      { 
                        columns: 
                        [
                          {text: 'Tipo:', style:'cabecera'}
                        ] 
                      },
                      {
                       columns: [
                        [{
                          text: datosConsulta.tipoActividad, style: 'estudiante'
                        }],
                       ]
                      },
                      {
                        columns: [          
                          {
                            table: {
                              headerRows: 1,
                              widths: ['*', '*','*','*'],
                    
                              body: [
                                [{
                                  text:'Documento estudiante',
                                  style: 'tableHeader'
                                }, 
                                {
                                  text:'Nombre estudiante',
                                  style: 'tableHeader'
                                },
                                {
                                  text:'Horas',
                                  style: 'tableHeader'
                                }
                              ],
                             ...datosActividad.map(i => {
                                return [i.students, i.nombreEstudiante,i.completedHours];
                              })
                             
                              ]
                            }, style:'tabla'
                          }
                        ]
                      },
                    ],
                    
                    styles: {
                      titulo: {
                        fontSize: 30,
                        bold: true,
                        alignment: 'center',
                        // margin: [left, top, right, bottom]
                        margin: [0, -90, 0, 10],
                        color:'#0553a7',
                      },
                      firma: {
                        fontSize: 13,
                        alignment: 'left',
                        margin: [0, 25, 0, 0],
                      },
                      tabla: {
                        fontSize: 9,
                        alignment: 'center',
                        margin: [0, 0, 0, 0],
                      },
                      datos: {
                        fontSize: 12,
                        alignment: 'left',
                        margin: [0, 0, 0, 0],
                      },
                      imagen: {
                        alignment: 'left'
                      },
                      tableHeader: {
                        bold: true,
                      },
                      cabecera: {
                        decoration: 'underline',
                        bold:true,
                        fontSize:15,
                        margin: [0, 5, 0, 5]
                      },
                
                      estudiante: {
                        margin: [0, 0, 0, 3],
                        fontSize: 11,
                      }
                    }
                  };
                  pdfMake.createPdf(documentDefinition).open();
            }else if(datosActividad.code==404){
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: datosActividad.description
                  })
            }
      }
    

      async  consultarReportEstudiante(id){
        let respuestaActividadesReport;
        await this.api.getReportActividadEstudiante(id)
            .toPromise().then(
            (response) => {
              respuestaActividadesReport = response;
                //console.log(respuestaActividadesReport)
            }, err => {
              respuestaActividadesReport=err.error;
            });
            return respuestaActividadesReport;
      }
      

      async getDatosStudent(student){
        let respuesta;
        await  this.api.getDatosUsuario(student)
              .toPromise().then(
                (response) => {
                respuesta = response;
              }, err => {
                respuesta=err.error;
              });
              return respuesta;
      }
      async getDatosActividad(id){
        let respuesta;
        await  this.api.getActividadesById(id)
              .toPromise().then(
                (response) => {
                respuesta = response;
              }, err => {
                respuesta=err.error;
              });
              return respuesta;
      }
      async  consultarDocumento(){
        let documento=this.document;
        let datosConsulta;
        let datosActividad;
        let x:number;
        await this.getDatosStudent(documento).then(response => {
          datosConsulta=response
        });
        await this.consultarReportEstudiante(documento).then(response => {
            datosActividad=response;
          });

         
          if(datosActividad.code==200){
            datosActividad=datosActividad.data;
            let datosEstudiante=datosConsulta.valor
            let nombreActividad;
            let nombre_estudiante=datosEstudiante.primerNombre+' '+datosEstudiante.segundoNombre+' '+datosEstudiante.primerApellido+' '+datosEstudiante.segundoApellido

            // console.log(nombreActividad)
             console.log(datosEstudiante)
            let totalHoras=0;
            for(let x=0 ;x<datosActividad.length;x++){
              totalHoras+=datosActividad[x].completedHours;
              await this.getDatosActividad(datosActividad[x].activities).then(response => {
                nombreActividad=response.valor.actividad;
              });
              
            datosActividad[x].nombreActividad=nombreActividad;
            }
            console.log(totalHoras)

            var documentDefinition = {
                content: [
                  {
                    image: this.logoDataUrl, style:'imagen'
                  },
                  { text: 'Reporte de actividad sumatoria', style: 'titulo' },
                  { columns: 
                    [
                      {text: 'Nombre estudiante:', style:'cabecera'},
                    ] 
                  },
                  {
                    columns: [
                     [{
                       text: nombre_estudiante, style: 'estudiante'
                     }],
                    ]
                   },
                   { columns: 
                    [
                      {text: 'Correo electrónico:', style:'cabecera'},
                    ] 
                  },
                  {
                    columns: [
                     [{
                       text: datosEstudiante.email, style: 'estudiante'
                     }],
                    ]
                   },
                   { columns: 
                    [
                      {text: 'Identificación:', style:'cabecera'},
                    ] 
                  },
                  {
                    columns: [
                     [{
                       text: datosEstudiante.identificacion, style: 'estudiante'
                     }],
                    ]
                   },
                   { columns: 
                    [
                      {text: 'Ciudad recidencia:', style:'cabecera'},
                    ] 
                  },
                  {
                    columns: [
                     [{
                       text: datosEstudiante.ciudadResidencia, style: 'estudiante'
                     }],
                    ]
                   },
                  { 
                    columns: 
                    [
                      {text: 'Dirección:', style:'cabecera'}
                    ] 
                  },
                  {
                   columns: [
                    [{
                      text: datosEstudiante.direccionResidencia, style: 'estudiante'
                    }],
                   ]
                  },
                  {
                    columns: [          
                      {
                        table: {
                          headerRows: 1,
                          widths: ['*', 100],
                
                          body: [
                            [{
                              text:'Actividad',
                              style: 'tableHeader'
                            }, 
                            {
                              text:'Horas',
                              style: 'tableHeader'
                            }
                          ],
                         ...datosActividad.map(i => {
                            return [i.nombreActividad, i.completedHours];
                          })
                         
                          ]
                        }, style:'tabla'
                      }
                    ]
                    
                  },
                  { columns: 
                    [
                      {text: 'Total horas realizadas: '+totalHoras, style:'cabecera'},
                    ] 
                  },
                ],
                
                styles: {
                  titulo: {
                    fontSize: 30,
                    bold: true,
                    alignment: 'center',
                    // margin: [left, top, right, bottom]
                    margin: [0, -90, 0, 10],
                    color:'#0553a7',
                  },
                  firma: {
                    fontSize: 13,
                    alignment: 'left',
                    margin: [0, 25, 0, 0],
                  },
                  tabla: {
                    fontSize: 9,
                    alignment: 'center',
                    margin: [0, 0, 0, 0],
                  },
                  datos: {
                    fontSize: 12,
                    alignment: 'left',
                    margin: [0, 0, 0, 0],
                  },
                  imagen: {
                    alignment: 'left'
                  },
                  tableHeader: {
                    bold: true,
                  },
                  cabecera: {
                    decoration: 'underline',
                    bold:true,
                    fontSize:15,
                    margin: [0, 5, 0, 5]
                  },
            
                  estudiante: {
                    margin: [0, 0, 0, 3],
                    fontSize: 11,
                  }
                }
              };
              pdfMake.createPdf(documentDefinition).open();
        }else if(datosActividad.code==404 && datosConsulta.codigo==404){
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: datosConsulta.mensaje
              })
        }else if(datosActividad.code==404 && datosConsulta.codigo==200){
          Swal.fire({
              icon: 'error',
              title: 'Oops...',
              text: datosActividad.description
            })
      }
      }


}
