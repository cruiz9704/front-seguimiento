import { Component, OnInit } from '@angular/core';

import { ActividadI } from '../../../modelos/actividad.interface';
import { ApiService } from '../../../sevicios/api/api.service';
import { Router,ActivatedRoute } from '@angular/router';

import { FormGroup, FormControl, Validators  } from '@angular/forms';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-registrar-actividad',
  templateUrl: './registrar-actividad.component.html',
  styleUrls: ['./registrar-actividad.component.css']
})
export class RegistrarActividadComponent implements OnInit {

  submitForm= new FormGroup({
    students : new FormControl('',Validators.required),
    nombre : new FormControl({ value: '', disabled: true },[Validators.required]),
    facultad : new FormControl({ value: '', disabled: true },Validators.required),
    programa : new FormControl({ value: '', disabled: true },Validators.required),
    activities :new FormControl('',Validators.required),
    actividad : new FormControl({ value: '', disabled: true },Validators.required),
    attendanceDate : new FormControl('',Validators.required),
    completedHours : new FormControl('',Validators.required),
  })

  constructor ( private api:ApiService, private router:Router,private activeroute:ActivatedRoute){}

  nombre:string = localStorage.getItem('full_name');
  pageid:string = localStorage.getItem('pageId');
  admin:string = localStorage.getItem('validateAdmin');
  BU:string = localStorage.getItem('validateBU');
  psicologo:string = localStorage.getItem('validatePsicologo');
  director:string = localStorage.getItem('validateDirector');
  docente:string = localStorage.getItem('validateDocente');
  ID:number;
  idActividad:string=this.activeroute.snapshot.paramMap.get('id');
  dataFamiliares:any=[{}];

  ngOnInit(): void {
   this.getActividadId(this.idActividad);
   this.validartoken();
  }

  validartoken(){
    if(!localStorage.getItem('token')){
       this.router.navigate(['login'])
    }
  }

  OnSubmit(form:ActividadI){
    let formActividad : ActividadI = form;
    console.log(form)
    console.log(formActividad)
    this.api.setActividad(formActividad).subscribe((data)=>{
      console.log(data)
     if(data.code==201){
       this.resetForm()
      Swal.fire(
        'Exito!',
        data.message,
        'success'
      )

     }
    }, err => {     
      Swal.fire(
        'Error!',
        err.error.description,
        'error'
      )
     });
  }

  resetForm(){
    this.submitForm.patchValue({
      students :'',
      nombre : '',
      facultad : '',
      programa : '',
      activities : '',
      actividad : '',
      attendanceDate : '',
      completedHours : '',
    })
  }

  
  getDatosPrograma(pegeid){
    this.api.getPrograma(pegeid).subscribe(
      (data)=>{
        if(data['valor'].length>0){
          this.submitForm.patchValue({
            facultad:"Ingeniera",
            programa:data['valor'][0].nombrePrograma
          })
        }else{
          this.submitForm.patchValue({
            facultad:"Ingeniera",
            programa:"No registra"
          })
        }
        
      });
  }
  
  getIdentificacion(event: any){
    this.ID=event.target.value;
    this.api.getDatosUsuario(this.ID).subscribe(
      (data)=>{            
        this.dataFamiliares=data.valor['familiares'];
        let nombre_completo= data.valor.primerNombre+' '+data.valor.primerApellido+' '+data.valor.segundoApellido;
        
        this.submitForm.patchValue({
          nombre:nombre_completo,
        })
        this.getDatosPrograma(data.valor.pegeId)
      },err => {     
        Swal.fire(
          'Error!',
          err.error.mensaje,
          'error'
        )
       });
  }
  getActividadId(id){
    this.api.getActividadesById(id).subscribe(
      (data)=>{ 
        this.submitForm.patchValue({
          activities:data.valor.idActividad,
          actividad:data.valor.actividad,
          
        })
      },err => {     
        Swal.fire(
          'Error!',
          'No se encontro actividad'+this.ID,
          'error'
        )
       });
   }

 /*getActividadId(event: any){
  this.ID=event.target.value;
  this.api.getActividadesById(this.ID).subscribe(
    (data)=>{ 
      this.submitForm.patchValue({
        actividad:data.valor.actividad,
      })
    },err => {     
      Swal.fire(
        'Error!',
        'No se encontro actividad con el id: '+this.ID,
        'error'
      )
     });
 }*/
}
