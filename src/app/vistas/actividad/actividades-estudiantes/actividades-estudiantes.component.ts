import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { ApiService } from '../../../sevicios/api/api.service';
import { Subject } from 'rxjs';
import Swal from 'sweetalert2'
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormControl, Validators  } from '@angular/forms';
import { DataTableDirective } from 'angular-datatables';

@Component({
  selector: 'app-actividades-estudiantes',
  templateUrl: './actividades-estudiantes.component.html',
  styleUrls: ['./actividades-estudiantes.component.css'],
  providers: [ApiService]
})
export class ActividadesEstudiantesComponent implements OnInit, OnDestroy {

  dtOptions: DataTables.Settings = {};
  dtTrigger = new Subject();
  data: any;
  estudiante="";
  constructor(private api: ApiService, private router:Router, private activeroute:ActivatedRoute) { }

  idActividad:string=this.activeroute.snapshot.paramMap.get('id');
  nameActividad:string=this.activeroute.snapshot.paramMap.get('name');

  submitForm= new FormGroup({
    nombre : new FormControl('',Validators.required)
  })

  ngOnInit(): void {
    var groupColumn = 1;
    this.dtOptions = {
        pagingType: 'full_numbers',
        pageLength: 10,
        ordering: true,
        processing: true,
        searching: true,
        columnDefs: [
            { "visible": false, "targets": groupColumn }
        ],
        order: [[groupColumn, 'asc']],
        language: { url: '//cdn.datatables.net/plug-ins/1.10.25/i18n/Spanish.json' },
        drawCallback: function (settings) {
          var api = this.api();
          var rows = api.rows({ page: 'current' }).nodes();
          var last = null;
          api.column(groupColumn, { page: 'current' }).data().each(function (group, i) {
              if (last !== group) {
                  $(rows).eq(i).before(
                      '<tr class="group"><td colspan="5" style="background-color:#0553a7; color: #fff; border: 1px solid #fff;"><b>' + group + '</b></td></tr>'
                  );
                  last = group;
              }
          });
      }
    }, err => {
        console.log(err);
    }
      this.getActividadesId(this.idActividad);
      this.validartoken();
  }

  @ViewChild(DataTableDirective, {static: false})
  dtElement: DataTableDirective;

  rerender(): void {
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      dtInstance.destroy();
    });
  }

  ngOnDestroy(): void {
      this.dtTrigger.unsubscribe();
  }

  validartoken(){
    if(!localStorage.getItem('token')){
       this.router.navigate(['login'])
    }
  }


  async getActividades() {
    await this.api.getActividadesSeguimiento()
        .subscribe((res: any) => {
            this.data = res.data;
            this.dtTrigger.next();
            console.log(this.data)
        }, err => {
            console.log(err);
        });
}

async getDataStudents(id){
let respuestaActividades;
await this.api.getDatosUsuario(id)
    .toPromise().then(
    (response) => {
        respuestaActividades = response;
        //console.log(respuestaActividades)
    }, err => {
        respuestaActividades=err.error;
    });
  return respuestaActividades;
}

async extraerData(id){
  let datosConsulta;
  await this.getDataStudents(id).then(response => {
    datosConsulta=response.valor.primerNombre+' '+response.valor.primerApellido+' '+response.valor.segundoApellido
  });
  console.log(datosConsulta)
  return datosConsulta;
}


async getActividadesId(id) {
  await this.api.getActividadesSeguimientoId(id)
      .subscribe((res: any) => {
          this.data = res.data;
          for(let x=0 ;x<this.data.length;x++){
            //this.getDataStudents( this.data[x].students)
            let nombre = this.extraerData(this.data[x].students);
            console.log(nombre);
            //this.data[x].nombre=nombre;
          }
          this.dtTrigger.next();
      }, err => {
        const swalWithBootstrapButtons = Swal.mixin({
          customClass: {
            confirmButton: 'btn btn-success'
          },
          buttonsStyling: false
        })
        
        swalWithBootstrapButtons.fire({
          title: err.error.description,
          icon: 'error',
          confirmButtonText: 'Regresar'
        }).then((result) => {
          if (result.isConfirmed) {
            this.router.navigate(['consultarActividad'])
          } 
        })
      });
}

editarActividad(id){  
  this.router.navigate(['editarActividad',id])
 }

 inactivarActividad(id){
  this.api.putActividadesEstudiantes(id).subscribe((data)=>{
    this.rerender();
   this.getActividadesId(this.idActividad)
   if(data['code']==201){
    Swal.fire(
      'Exito!',
      data['message'],
      'success'
    )
    //this.router.navigate(['editarActividad',id])
   }
  }, err => {     
    Swal.fire(
      'Error!',
      'Datos ingresados erroneamente',
      'error'
    )
   });
}

 confirmacion(id){
   console.log(id)
  Swal.fire({
    title: '¿Estas seguro de eliminar la actividad?',
    text: "una vez eliminada no podras revertir esta acción!",
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Si, eliminar!'
  }).then((result) => {
    if (result.isConfirmed) {
      this.inactivarActividad(id);
      /*Swal.fire(
        'Deleted!',
        'Your file has been deleted.',
        'success'
      )*/
    }
  })
 }
       

}
