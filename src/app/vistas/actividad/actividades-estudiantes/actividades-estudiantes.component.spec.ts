import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ActividadesEstudiantesComponent } from './actividades-estudiantes.component';

describe('ActividadesEstudiantesComponent', () => {
  let component: ActividadesEstudiantesComponent;
  let fixture: ComponentFixture<ActividadesEstudiantesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ActividadesEstudiantesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActividadesEstudiantesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
