import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConsultaPsicologicaComponent } from './consulta-psicologica.component';

describe('ConsultaPsicologicaComponent', () => {
  let component: ConsultaPsicologicaComponent;
  let fixture: ComponentFixture<ConsultaPsicologicaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConsultaPsicologicaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConsultaPsicologicaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
