import { Component, OnInit } from '@angular/core';

import { PsicologiaI } from '../../../modelos/pisocologia.interface';
import { ApiService } from '../../../sevicios/api/api.service';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';
import { FormGroup, FormControl, Validators  } from '@angular/forms';

@Component({
  selector: 'app-consulta-psicologica',
  templateUrl: './consulta-psicologica.component.html',
  styleUrls: ['./consulta-psicologica.component.css'],
  providers:[ApiService]
})
export class ConsultaPsicologicaComponent implements OnInit {

    nombre:string = localStorage.getItem('full_name');
    pageid:string = localStorage.getItem('pageId');
    identificacion:string = localStorage.getItem('identificacion');
    admin:string = localStorage.getItem('validateAdmin');
    BU:string = localStorage.getItem('validateBU');
    psicologo:string = localStorage.getItem('validatePsicologo');
    director:string = localStorage.getItem('validateDirector');
    docente:string = localStorage.getItem('validateDocente');
    ID:number;
    dataFamiliares:any=[{}];

    submitForm= new FormGroup({
    students : new FormControl('',Validators.required),
    users : new FormControl(this.identificacion,Validators.required),
    nombre : new FormControl({ value: '', disabled: true },Validators.required),
    edad : new FormControl({ value: '', disabled: true },Validators.required),
    nacimiento : new FormControl({ value: '', disabled: true },Validators.required),
    genero : new FormControl({ value: '', disabled: true },Validators.required),
    direccion : new FormControl({ value: '', disabled: true },Validators.required),
    telefono : new FormControl({ value: '', disabled: true },Validators.required),
    email : new FormControl({ value: '', disabled: true },Validators.required),
    creationDate : new FormControl('',Validators.required),
    description : new FormControl('',Validators.required),
    reason : new FormControl('',Validators.required),
    factors : new FormControl('',Validators.required),
    diagnosis : new FormControl('',Validators.required),
    familyBackground : new FormControl('',Validators.required),
    facultad : new FormControl({ value: '', disabled: true },Validators.required),
    semestre : new FormControl({ value: '', disabled: true },Validators.required),
    programa : new FormControl({ value: '', disabled: true },Validators.required),
  })

  constructor ( private api:ApiService, private router:Router){}

  ngOnInit(): void {
      this.validartoken();
  }

  validartoken(){
    if(!localStorage.getItem('token')){
       this.router.navigate(['login'])
    }
  }
  OnSubmit(form:PsicologiaI){
    console.log(form)
    this.api.setPsicologia(form).subscribe((data)=>{
      if(data.code==201){
        this.resetForm()
       Swal.fire(
         'Exito!',
         data.message,
         'success'
       )
 
      }
    },err => {     
      console.log(err)
      Swal.fire(
        'Error!',
        err.error.description,
        'error'
      )
     });
    let dataPsicologia : PsicologiaI = form;
  
  }

  resetForm(){
    this.submitForm.patchValue({
      students : '',
      nombre : '',
      edad : '',
      nacimiento : '',
      genero : '',
      direccion : '',
      telefono : '',
      email : '',
      creationDate : '',
      description : '',
      reason : '',
      factors : '',
      diagnosis : '',
      familyBackground : '',
      facultad : '',
      semestre : '',
      programa : ''
    })
  }
   calcularEdad(fecha) {
    var hoy = new Date();
    var cumpleanos = new Date(fecha);
    var edad = hoy.getFullYear() - cumpleanos.getFullYear();
    var m = hoy.getMonth() - cumpleanos.getMonth();

    if (m < 0 || (m === 0 && hoy.getDate() < cumpleanos.getDate())) {
        edad--;
    }

    return edad;
  }
  

  getDatosPrograma(pegeid){
    this.api.getPrograma(pegeid).subscribe(
      (data)=>{
        if(data['valor'].length>0){
          let programa=data['valor'][0].nombrePrograma;
          let semestre=data['valor'][0].semestre;
          this.submitForm.patchValue({
            facultad:"Ingeniera",
            programa:programa,
            semestre:semestre
          })
        }else{
          this.submitForm.patchValue({
            facultad:"Ingeniera",
            programa:"No registra",
            semestre:"No registra"
          })
        }
        
      });
  }
  
  getIdentificacion(event: any){
    this.ID=event.target.value;
    this.api.getDatosUsuario(this.ID).subscribe(
      (data)=>{            
        this.dataFamiliares=data.valor['familiares'];
        let nombre_completo= data.valor.primerNombre+' '+data.valor.primerApellido+' '+data.valor.segundoApellido;
        let sexo = data.valor.sexo;
        let fecha = data.valor.fechaNacimiento;
        let edad = this.calcularEdad(fecha);
        let direccion = data.valor.direccionResidencia+", "+data.valor.ciudadResidencia;
        let telefono = data.valor.telefono;
        let email = data.valor.email;
        console.log(edad)
        if(sexo=='M'){
          sexo="Masculino"
        }else if(sexo=='F'){
          sexo="Femenino"
        }
        
        this.submitForm.patchValue({
          nombre:nombre_completo,
          genero:sexo,
          nacimiento: fecha,
          edad: edad,
          direccion:direccion,
          telefono:telefono,
          email:email
        })
        this.getDatosPrograma(data.valor.pegeId)
      },
      //(error)=>{ console.log(error.error.mensaje)}
      err => {     
        Swal.fire(
          'Error!',
          err.error.mensaje,
          'error'
        )
       }
      )
  }

  onBlurEvent(event: any){

    console.log(event.target.value);
 
 }


}
