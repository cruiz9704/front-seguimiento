import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from '../../../sevicios/api/api.service';
import { Observable, Subject } from 'rxjs';
import { ThrowStmt } from '@angular/compiler';

import pdfMake from 'pdfmake/build/pdfmake';
import pdfFonts from 'pdfmake/build/vfs_fonts';
import { Utils } from 'src/app/utils/utils';
import { bufferToggle } from 'rxjs/operators';
import Swal from 'sweetalert2';
import { DataTableDirective } from 'angular-datatables';

pdfMake.vfs = pdfFonts.pdfMake.vfs;

@Component({
  selector: 'app-consultar-citas',
  templateUrl: './consultar-citas.component.html',
  styleUrls: ['./consultar-citas.component.css']
})
export class ConsultarCitasComponent implements OnInit {

  logoDataUrl: string;
  nombre:string = localStorage.getItem('full_name');
  pageid:string = localStorage.getItem('pageId');
  identificacion:string = localStorage.getItem('identificacion');
  admin:string = localStorage.getItem('validateAdmin');
  BU:string = localStorage.getItem('validateBU');
  psicologo:string = localStorage.getItem('validatePsicologo');
  director:string = localStorage.getItem('validateDirector');
  docente:string = localStorage.getItem('validateDocente');
  
  dtOptions: DataTables.Settings = {};
    dtTrigger = new Subject();
    data: any;
    datosPiscologia:any;
    subject = new Subject<any>();
    constructor(private api: ApiService, private router:Router) { }

    ngOnInit(): void {
      var groupColumn = 1;
      this.dtOptions = {
          pagingType: 'full_numbers',
          pageLength: 10,
          ordering: true,
          processing: true,
          searching: true,
          columnDefs: [
              { "visible": false, "targets": groupColumn }
          ],
          order: [[groupColumn, 'asc']],
          language: { url: '//cdn.datatables.net/plug-ins/1.10.25/i18n/Spanish.json' },
          drawCallback: function (settings) {
            var api = this.api();
            var rows = api.rows({ page: 'current' }).nodes();
            var last = null;
            api.column(groupColumn, { page: 'current' }).data().each(function (group, i) {
                if (last !== group) {
                    $(rows).eq(i).before(
                        '<tr class="group"><td colspan="5" style="background-color:#0553a7; color: #fff; border: 1px solid #fff;"><b>' + group + '</b></td></tr>'
                    );
                    last = group;
                }
            });
        }
      }, err => {
          console.log(err);
      }
        this.getReportPsicologia(this.identificacion)

        Utils.getImageDataUrlFromLocalPath1('assets/img/letras_azul.png').then(
          result => this.logoDataUrl = result
        )
        this.validartoken();
    }

    @ViewChild(DataTableDirective, {static: false})
    dtElement: DataTableDirective;
  
    rerender(): void {
      this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
        dtInstance.destroy();
      });
    }

    validartoken(){
      if(!localStorage.getItem('token')){
         this.router.navigate(['login'])
      }
    }

    ngOnDestroy(): void {
        this.dtTrigger.unsubscribe();
    }

    async getReportPsicologia(id){
      await  this.api.getPsicologiaByUser(id)
            .subscribe((res: any) => {
                this.data = res.data;
                this.dtTrigger.next();
            }, err => {
                console.log(err);
            });

  }

  calcularEdad(fecha) {
    var hoy = new Date();
    var cumpleanos = new Date(fecha);
    var edad = hoy.getFullYear() - cumpleanos.getFullYear();
    var m = hoy.getMonth() - cumpleanos.getMonth();

    if (m < 0 || (m === 0 && hoy.getDate() < cumpleanos.getDate())) {
        edad--;
    }

    return edad;
  }

  async getDatosStudent(student){
    let respuesta;
    await  this.api.getDatosUsuario(student)
          .toPromise().then((response) => {
            respuesta = response;
          }).catch(e => console.error(e));
          return respuesta;
  }

  async geDataReportByOne(id,student,user){
    let respuesta;
    await  this.api.getPsicologiaReportByOne(id,student,user)
          .toPromise().then((response) => {
            respuesta = response;
          }).catch(e => console.error(e));
          return respuesta;
  }
  async getDatosPrograma(pegeid){
    let respuesta
    await this.api.getPrograma(pegeid).toPromise().then((response) => {
      respuesta = response;
    }).catch(e => console.error(e));
    return respuesta;
  }

  async openPdfReport(id,student,user){
    let datosConsulta;
    let datosEstudiante;
    let sexo;
    let edad;
    let familia;
    let programa;
    //trae informacion de la consulta psicologica
    await this.geDataReportByOne(id,student,user).then(response => {
      datosConsulta=response.data[0]
    });

    //trae informacion del estudiante
    await this.getDatosStudent(student).then(response => {
      datosEstudiante=response.valor
    });

    await this.getDatosPrograma(datosEstudiante.pegeId).then(response => {
      programa=response.valor[0]
    });

    familia = datosEstudiante['familiares']
    edad = this.calcularEdad(datosEstudiante.fechaNacimiento);
    
    if(datosEstudiante.sexo='M'){
      sexo="Masculino"
    }else if(datosEstudiante.sexo='F'){
      sexo="Femenino"
    }

  var documentDefinition = {
    userPassword: datosEstudiante.identificacion,
    ownerPassword: datosConsulta.users,
    content: [
      {
        image: this.logoDataUrl, style:'imagen'
      },
      { text: 'Reporte Psicologico', style: 'titulo' },
      { columns: 
        [
          {text: 'Datos Personales:', style:'cabecera'},
          {text: 'Grupo Familiar:', style:'cabecera'},
        ] 
      },
      {
        columns: [
          [{
            text: 'Fecha entrevista :' + datosConsulta.creationDate, style: 'estudiante'
          },{
            text: 'Documento : ' + datosEstudiante.identificacion, style: 'estudiante'
          },
          {
            text: 'Nombre : ' + datosEstudiante.primerNombre+' '+datosEstudiante.primerApellido+' '+datosEstudiante.segundoApellido, style: 'estudiante'
          },
          {
            text: 'Sexo :' + sexo, style: 'estudiante'
          },
          {
            text: 'Fecha Nacimiento :' + datosEstudiante.fechaNacimiento, style: 'estudiante'
          },
          {
            text: 'Edad :' + edad, style: 'estudiante'
          },
          {
            text: 'Telefono :' + datosEstudiante.telefono, style: 'estudiante'
          },
          {
            text: 'Dirección :' + datosEstudiante.direccionResidencia+', '+datosEstudiante.ciudadResidencia, style: 'estudiante'
          },
          
          {text: 'Datos Academicos:', style:'cabecera'},
          {
            text: 'Programa :' + programa.nombrePrograma, style: 'estudiante'
          },
          {
            text: 'Semestre :' + programa.semestre, style: 'estudiante'
          },
          ],

          
          {
            table: {
              headerRows: 1,
              widths: ['*', '*','*','*'],
    
              body: [
                [{
                  text:'Nombre',
                  style: 'tableHeader'
                }, 
                {
                  text:'Parentesco',
                  style: 'tableHeader'
                },
                {
                  text:'Direccion',
                  style: 'tableHeader'
                },
                {
                  text:'Telefono',
                  style: 'tableHeader'
                },
              ],
              ...familia.map(i => {
                return [i.nombreFamiliar, i.parentesco, i.dirFamiliar, i.telFamiliar];
              })
              ]
            }, style:'tabla'
          }
        ]
      },

      { 
        columns: 
        [
          {text: 'Descripción:', style:'cabecera'}
        ] 
      },
      {
       columns: [
        [{
          text: datosConsulta.description, style: 'datos'
        }],
       ]
      },

      { 
        columns: 
        [
          {text: 'Factores:', style:'cabecera'}
        ] 
      },
      {
       columns: [
        [{
          text: datosConsulta.factors, style: 'datos'
        }],
       ]
      },
      
      { 
        columns: 
        [
          {text: 'Motivo:', style:'cabecera'}
        ] 
      },
      {
       columns: [
        [{
          text: datosConsulta.reason, style: 'datos'
        }],
       ]
      },

      { 
        columns: 
        [
          {text: 'Antecedentes:', style:'cabecera'}
        ] 
      },
      {
       columns: [
        [{
          text: datosConsulta.familyBackground, style: 'datos'
        }],
       ]
      },

      { 
        columns: 
        [
          {text: 'Diagnostico:', style:'cabecera'}
        ] 
      },
      {
       columns: [
        [{
          text: datosConsulta.diagnosis, style: 'datos'
        }],
       ]
      },
      { 
        columns: 
        [
          {text: 'Realizado por:', style:'firma'}
        ] 
      },
      { 
        columns: 
        [
          {text: '_____________________________', style:'firma'}
        ] 
      },
      { 
        columns: 
        [
          {text: this.nombre, style:'tableHeader'}
        ] 
      },
      { 
        columns: 
        [
          {text: "C.C: "+datosConsulta.users, style:'tableHeader'}
        ] 
      },
    ],
    
    styles: {
      titulo: {
        fontSize: 30,
        bold: true,
        alignment: 'center',
        // margin: [left, top, right, bottom]
        margin: [0, -90, 0, 10],
        color:'#0553a7',
      },
      firma: {
        fontSize: 13,
        alignment: 'left',
        margin: [0, 25, 0, 0],
      },
      tabla: {
        fontSize: 9,
        alignment: 'center',
        margin: [-13, 0, 0, 0],
      },
      datos: {
        fontSize: 12,
        alignment: 'left',
        margin: [0, 0, 0, 0],
      },
      imagen: {
        alignment: 'left'
      },
      tableHeader: {
        bold: true,
      },
      fecha: {
        bold: true,
        decoration: 'underline',
      },
      cabecera: {
        decoration: 'underline',
        bold:true,
        fontSize:18,
        margin: [0, 13, 0, 5]
      },

      estudiante: {
        margin: [0, 0, 0, 3],
        fontSize: 12,
      }
    }
  };
  pdfMake.createPdf(documentDefinition).open();
  }

  CerrarEntrevista(id){
    this.api.putCerrarEntrevista(id).subscribe((data)=>{
     if(data['code']==201){
      Swal.fire(
        'Exito!',
        data['message'],
        'success'
      )
      this.rerender()
      this.getReportPsicologia(this.identificacion);
     }
    }, err => {     
      Swal.fire(
        'Error!',
        err.error.description,
        'error'
      )
     });
  }
  
   confirmacion(id){
    Swal.fire({
      title: '¿Estas seguro de cerrar esta entrevista?',
      text: "una vez cerrada no podras revertir esta acción!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, cerrar!'
    }).then((result) => {
      if (result.isConfirmed) {
        this.CerrarEntrevista(id);
      }
    })
   }
   

}
