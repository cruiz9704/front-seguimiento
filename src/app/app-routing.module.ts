import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LoginComponent } from './vistas/login/login.component';
import { DashboardComponent } from './vistas/dashboard/dashboard.component'
import { RegistrarActividadComponent } from './vistas/actividad/registrar-actividad/registrar-actividad.component';
import { ConsultaPsicologicaComponent } from './vistas/psicologia/consulta-psicologica/consulta-psicologica.component';
import { CrearTutoriaComponent } from './vistas/tutorias/crear-tutoria/crear-tutoria.component';
import { ConsultarActividadesComponent } from './vistas/actividad/consultar-actividades/consultar-actividades.component';
import { ConsultarTurotiasComponent } from './vistas/tutorias/consultar-turotias/consultar-turotias.component';
import { ConsultarCitasComponent } from './vistas/psicologia/consultar-citas/consultar-citas.component';
import { GestionarTutoriaComponent } from './vistas/tutorias/gestionar-tutoria/gestionar-tutoria.component';
import { EditarTutoriaComponent } from './vistas/tutorias/editar-tutoria/editar-tutoria.component';
import { ActividadesEstudiantesComponent } from './vistas/actividad/actividades-estudiantes/actividades-estudiantes.component';
import { HorarioTutoriaComponent } from './vistas/tutorias/horario-tutoria/horario-tutoria.component';
import { EditarActividadComponent } from './vistas/actividad/editar-actividad/editar-actividad.component';

const routes: Routes = [
  { path:'', redirectTo:'login', pathMatch:'full'},
  { path:'login', component:LoginComponent},
  { path:'dashboard', component:DashboardComponent},
  { path:'registrarActividad/:id', component:RegistrarActividadComponent},
  { path:'consulta', component:ConsultaPsicologicaComponent},
  { path:'crearTutoria', component:CrearTutoriaComponent},
  { path:'consultarActividad', component:ConsultarActividadesComponent},
  { path:'consultarTutoria', component:ConsultarTurotiasComponent},
  { path:'consultarCitas', component:ConsultarCitasComponent},
  { path:'gestionarTutoria/:id', component:GestionarTutoriaComponent},
  { path:'editarTutoria', component:EditarTutoriaComponent},
  { path:'actividadesEstudiantes/:id/:name', component:ActividadesEstudiantesComponent},
  { path:'horarioTutorias/:id', component:HorarioTutoriaComponent},
  { path:'editarActividad/:id', component:EditarActividadComponent},
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
export const routingComponents = [
  LoginComponent, 
  DashboardComponent,
  RegistrarActividadComponent,
  ConsultaPsicologicaComponent,
  CrearTutoriaComponent,
  ConsultarActividadesComponent,
  ConsultarTurotiasComponent,
  ConsultarCitasComponent,
  GestionarTutoriaComponent,
  EditarTutoriaComponent,
  ActividadesEstudiantesComponent,
  HorarioTutoriaComponent,
  EditarActividadComponent
];
