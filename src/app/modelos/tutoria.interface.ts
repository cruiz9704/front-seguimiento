export interface TutoriaI{
    users: number;
    timeZone: string;
    schedule: string;
    description: string;
}