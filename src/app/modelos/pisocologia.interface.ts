export interface PsicologiaI{
    reason: string;
    description: string;
    factors: String;
    familyBackground: string;
    diagnosis: string;
    users: string;
    students: string;
    creationDate : String;
}

export interface StudentI{
    reason: string;
    description: string;
    factors: String;
    familyBackground: string;
    diagnosis: string;
    users: string;
    students: string;
    creationDate : String;
}